#include "../partials/common/errors.religo"
#include "../partials/common/CommonHelpers.religo"
#include "../partials/types/IRewardsVault.religo"

// Helper function to get fa1.2 token contract
let getFa12TokenContract = ((token_address) : (address)) : contract(entry_fa12_type) => {
  unopt((Tezos.get_entrypoint_opt("%transfer", token_address) : option(contract(entry_fa12_type))), "wrong-12token-ep");
};
// Helper function to get fa2 token contract
let getFa2TokenContract = ((token_address) : (address)) : contract(entry_fa2_type) => {
  unopt((Tezos.get_entrypoint_opt("%transfer", token_address) : option(contract(entry_fa2_type))), "wrong-2token-ep");
};
// Helper function to transfer the asset based on its standard
let typedTransfer = ((owner, receiver, amount_, token) : (address, address, nat, token_t)) : list(operation) => {
  switch(token) {
    | Fa12 (token_address) => {
      [Tezos.transaction(
        TransferTypeFA12(owner, (receiver, amount_)),
        0mutez,
        getFa12TokenContract(token_address)
      )];
    }
    | Fa2 (token_info) => {
      [Tezos.transaction(
        TransferTypeFA2([{
            from_ : owner,
            txs : [{
              to_           : receiver,
              token_id      : token_info.id,
              amount        : amount_
            }]
        }]),
        0mutez,
        getFa2TokenContract(token_info.address)
      )];
    }
    | Tez => {
      if (receiver == Tezos.self_address) {
        ([] : list(operation));
      } else {
        [Tezos.transaction(
          unit,
          amount_ * 1mutez,
          (Tezos.get_contract_with_error(receiver, RewarderCore.cant_get_snd_ctrt) : contract(unit))
        )];
      }
    }
  }
};


let depositReward = ((p, s) : (RewarderTypes.deposit_params_t, storage_t)) : return_t => {
  // let reward_amt = p.reward[1];
  // let pool_reward_amt = p.pool_reward;

  // assert_with_error(reward_amt > pool_reward_amt, RewarderCore.total_lower_than_pool_rew);

  // let token = p.reward[0];
  // let user = p.user;
  // let user_reward_id = (user, token);
  // let user_reward_amt = abs(reward_amt - pool_reward_amt);

  // let old_user_reward = unoptOr(Big_map.find_opt(user_reward_id, s.user_reward), 0n);
  // let old_pool_reward = unoptOr(Big_map.find_opt(token, s.pool_reward), 0n);

  // let new_user_reward = old_user_reward + user_reward_amt;
  // let new_pool_reward = old_pool_reward + pool_reward_amt;

  // let user_reward = Big_map.update(user_reward_id, Some (new_user_reward), s.user_reward);
  // let pool_reward = Big_map.update(token, Some (new_pool_reward), s.pool_reward);

  // let new_s = { ...s, user_reward : user_reward, pool_reward : pool_reward };
  // let ops = typedTransfer(Tezos.sender, Tezos.self_address, reward_amt, token);
  // (ops, new_s);
};
let withdrawReward = ((p, s) : (RewarderTypes.withdraw_params_t, storage_t)) : return_t => {
  // let withdraw_amt = p.amount[1];
  // let token = p.amount[0];
  // let user = Tezos.sender;
  // let user_reward_id = (user, token);
  // let user_balance = unoptOr(Big_map.find_opt(user_reward_id, s.user_reward), 0n);

  // assert_with_error(withdraw_amt <= user_balance, RewarderCore.insufficient_balance);

  // let new_user_reward = abs (user_balance - withdraw_amt);
  // let user_reward = Big_map.update(user_reward_id, Some (new_user_reward), s.user_reward);
  // let receiver = unoptOr(p.receiver, user);

  // let ops = typedTransfer(Tezos.self_address, receiver, withdraw_amt, token);
  // (ops, { ...s, user_reward : user_reward })
};

let noOps = ([] : list (operation));

let main = ((p, store) : (actions, storage_t)) : return_t => {
  switch(p) {
    | DepositReward (params) => depositReward(params, store)
    | WithdrawReward (params) => withdrawReward(params, store)
    | SetDelegate (d) => {
      onlyAdmin(store.admin);
      let op = Tezos.set_delegate(Some (d));
      ([op], store);
    }
    // | Transfer (params) => callToken(params, store)
    // | Balance_of (params) => callToken(params, store)
    // | Update_operators (params) => callToken(params, store)
  };
};