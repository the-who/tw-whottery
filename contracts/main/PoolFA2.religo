#define FA2_STANDARD_ENABLED
#include "../partials/Pool.religo"

// PoolFA2 - Contract for pools for XTZ - FA2 token pair
let main = ((p, full_ps) : (full_pool_action, full_pool_storage_type)) : full_return_type => {
  let this : address = Tezos.self_address;
  switch(p) {
    | Default                            => useDefault(full_ps)
    | Use (params)                       => callPool(params, this, full_ps)
    | Admin (params)                     => callAdminFull(params, full_ps)
    | Transfer (params)                  => callToken(ITransfer(params), this, 0n, full_ps)
    | Balance_of (params)                => callToken(IBalance_of(params), this, 2n, full_ps)
    | Update_operators (params)          => callToken(IUpdate_operators(params), this, 1n, full_ps)
    | Get_reserves (params)              => getReserves(params, full_ps)
  };
};
