#include "../partials/Pool.religo"

// PoolA12 - Contract for pools for XTZ - FA1.2 token pair
let main = ((p, full_ps) : (full_pool_action, full_pool_storage_type)) : full_return_type => {
  let this : address = Tezos.self_address;
  switch(p) {
    | Default                    => useDefault(full_ps)
    | Use (params)               => callPool(params, this, full_ps)
    | Admin (params)             => callAdminFull(params, full_ps)
    | Transfer (params)          => callToken(ITransfer(params), this, 0n, full_ps)
    | Approve (params)           => callToken(IApprove(params), this, 1n, full_ps)
    | GetBalance (params)        => callToken(IGetBalance(params), this, 2n, full_ps)
    | GetAllowance (params)      => callToken(IGetAllowance(params), this, 3n, full_ps)
    | GetTotalSupply (params)    => callToken(IGetTotalSupply(params), this, 4n, full_ps)
    | GetReserves (params)       => getReserves(params, full_ps)
  };
};
