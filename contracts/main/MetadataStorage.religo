#include "../partials/IMetadataStorage.religo"

let noOps = ([] : list (operation)) ;

// Add or remove the admin permissions for address; only called by one of the current owners
let update_owner = ((params, s) : (update_owner_type, storage)) : return => {
  if (Set.mem(Tezos.sender, s.owners)) {
    if (params.add) {
      let new_st = { ...s, owners : Set.add (params.owner, s.owners) };
      (noOps, new_st);
    } else {
      let new_st = { ...s, owners : Set.remove (params.owner, s.owners) };
      (noOps, new_st);
    };
  } else {
    (failwith("MetadataStorage/permision-denied") : return);
  };
};

// Update the metadata for the token; only called by one of the current owners
let update_metadata = ((new_metadata, s) : (metadata_type, storage)) : return => {
  if (Set.mem(Tezos.sender, s.owners)) {
    let new_st = { ...s, metadata : new_metadata };
    (noOps, new_st);
  } else {
    (failwith("MetadataStorage/permision-denied") : return);
  }
};

// MetadataStorage - Contract to store and upgrade the shares token metadata
let main = ((p, s) : (storage_action, storage)) : return => {
  switch(p) {
    | Update_owners(params)         => update_owner(params, s)
    | Update_storage(new_metadata)  => update_metadata(new_metadata, s)
    | Get_metadata(receiver)        => ([Tezos.transaction(s.metadata, 0tz, receiver)], s)
  };
};