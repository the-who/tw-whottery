/*
  Accept Quipu FA12 shares, claim reward on Crunchy every round.
*/

#define ACCEPT_TOKENS_ENABLED // Accept Quipu shares
#define CLAIM_CRUNCHY_ENABLED

#include "../../partials/Factory.religo"