/*
  Accept Quipu FA2 shares, claim reward on Crunchy every round.
*/

#define FA2_STANDARD_ENABLED // switch the standard
#define ACCEPT_TOKENS_ENABLED // Accept Quipu shares
#define CLAIM_CRUNCHY_ENABLED // Claim source

#include "../../partials/Factory.religo"

