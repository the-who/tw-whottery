/*
  Accept Quipu FA2 shares, claim reward on Quipu every round.
*/

#define TEST_ENABLED

#define FA2_STANDARD_ENABLED // switch the standard
#define ACCEPT_TOKENS_ENABLED // Accept Quipu shares
#define CLAIM_QUIPU_ENABLED // Claim source

#include "../../partials/Factory.religo"

