/*
  Factory deploy's pool that:
  - Accept Quipu FA12 shares
  - claim reward on Quipu every round
  - draw
*/

#define TEST_ENABLED

#define ACCEPT_TOKENS_ENABLED // Accept Quipu shares
#define CLAIM_QUIPU_ENABLED

#include "../../partials/Factory.religo"