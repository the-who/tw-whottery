#if CLAIM_QUIPU_ENABLED

let createPool : create_pool_func =
[%Michelson ( {| { UNPPAIIR ;
                  CREATE_CONTRACT
#if FA2_STANDARD_ENABLED
#include "../compiled/PoolFA2_S2Q.tz"
#else
#include "../compiled/PoolFA12_S2Q.tz"
#endif
        ;
          PAIR } |}
 : create_pool_func)];

#endif

#if CLAIM_CRUNCHY_ENABLED

let createPool : create_pool_func =
[%Michelson ( {| { UNPPAIIR ;
                  CREATE_CONTRACT
#if FA2_STANDARD_ENABLED
#include "../compiled/PoolFA2_S2Cr.tz"
#else
#include "../compiled/PoolFA12_S2Cr.tz"
#endif
        ;
          PAIR } |}
 : create_pool_func)];

#endif

#if ACCEPT_TEZTOK_ENABLED

let createPool : create_pool_func =
[%Michelson ( {| { UNPPAIIR ;
                  CREATE_CONTRACT
#if FA2_STANDARD_ENABLED
#include "../compiled/PoolTezFA2_S2Loto.tz"
#else
#include "../compiled/PoolTezFA12_S2Loto.tz"
#endif
        ;
          PAIR } |}
 : create_pool_func)];

#endif