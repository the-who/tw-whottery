#include "./common/Errors.religo"
#include "./common/CommonHelpers.religo"
#include "./types/IPool.religo"
#include "./common/Admin.religo"
#include "./common/PoolHelpers.religo"
#include "./PoolViews.religo"

/*
  Route exchange-specific action
  Due to the fabulous storage, gas and operation size limits
  the only way to have all the nessary functions is to store
  them in big_map and dispatch the exact function code before
  the execution.

  The function is responsible for fiding the appropriate method
  based on the argument type.
*/

let callPool = ((p, this, full_ps) : (pool_action, address, full_pool_storage_type)) : full_return_type => {
  isPaused(full_ps.admin_storage);
  let idx : nat = switch(p) {
    | StartPool (_n) => {
      isAdmin(full_ps.admin_storage); // Only admin can call this EP
      Ids.start_pool;
    }
    | EnterPool (_n) => Ids.enter_pool
    | ExitPool (_n) => Ids.exit_pool
    | Draw (_n) => Ids.draw
    // | WithdrawReward (_n) => 4n
    | Round (_n) => Ids.round
  };
  let lambda_bytes = unopt(Big_map.find_opt(idx, full_ps.lambdas), PoolCore.err_unknown_func);
  let res : return_type = switch(Bytes.unpack(lambda_bytes) : option(pool_func)) {
    | Some (f) => f(p, full_ps.storage, this)
    | None => (failwith("Pool/function-not-set") : return_type)
  };
  let new_full_pool_st = { ...full_ps, storage : res[1] };
  (res[0], new_full_pool_st);
};

/*
Route token-specific action
The function is responsible for fiding the appropriate method
based on the provided index.
*/
let callToken = ((p, this, idx, full_ps) : (token_action, address, nat, full_pool_storage_type)) : full_return_type => {
  isPaused(full_ps.admin_storage);
  let lambda_bytes = unopt(Big_map.find_opt(idx, full_ps.token_lambdas_b), PoolCore.err_unknown_func);
  let res : return_type = switch(Bytes.unpack(lambda_bytes) : option(token_func)) {
    | Some (f) => f(p, full_ps.storage, this)
    | None     => (failwith("Pool/token-function-not-set") : return_type)
  };
  let new_full_ps = { ...full_ps, storage : res[1] };
  (res[0], new_full_ps);
};

let callAdmin = ((p, full_ps) : (admin_actions, full_pool_storage_type)) : full_return_type => {
  isAdmin(full_ps.admin_storage);
  let new_admin_st = handleAdminAction(p, full_ps.admin_storage);
  let new_full_ps = {
    ...full_ps,
    admin_storage : new_admin_st
  };
  (noOps, new_full_ps);
};

let callPoolAdmin = ((p, this, full_ps) : (pool_admin_action, address, full_pool_storage_type)) : full_return_type => {
  isAdmin(full_ps.admin_storage);
  let idx = switch(p) {
    | SetPrnSrc (_n)         => Ids.set_prn_src
    | SetRoundTimeframe (_n) => Ids.set_round_timeframe
    | SetRewardContract (_n) => Ids.set_reward_ctrt
  };

  let lambda_bytes = unopt(Big_map.find_opt(idx, full_ps.lambdas), PoolCore.err_unknown_func);
  let res : return_type = switch((Bytes.unpack(lambda_bytes) : option(admin_func))) {
    | Some (f) => f(p, full_ps.storage, this)
    | None => (failwith(PoolCore.cant_unpack_fn) : return_type)
  };
  // let res : return_type = switch(Big_map.find_opt(idx, full_ps.admin_lambdas)) {
  //   | Some (f) => f(p, full_ps.storage, this)
  //   | None => (failwith("Pool/admin-function-not-set") : return_type)
  // };
  let new_full_ps = { ...full_ps, storage : res[1] };
  (res[0], new_full_ps);
};

let callAdminFull = ((p, full_ps) : (full_admin_action, full_pool_storage_type)) : full_return_type => {
  let res = switch (p) {
    | CallAdmin (params)     => callAdmin(params, full_ps)
    | CallPoolAdmin (params) => callPoolAdmin(params, Tezos.self_address, full_ps)
  };
  res;
};

// Route the simple XTZ transfers
let useDefault = ((full_ps) : (full_pool_storage_type)) : full_return_type => {
  // let res : return_type = switch(Big_map.find_opt(pool_func_count, full_ps.lambdas)) {
  //   | Some (f) => f(StartPool(0n), full_ps.storage, Tezos.self_address)
  //   | None     => (noOps, full_ps.storage)
  // };
  // let new_full_pool_st = { ...full_ps, storage : res[1] };
  // (res[0], new_full_pool_st) ;
  (noOps, full_ps);
};

let getReserves = ((receiver, full_ps) : (contract((nat, nat)), full_pool_storage_type)) : full_return_type => {
  ([
    Tezos.transaction((
      full_ps.storage.tezos_pool,
      full_ps.storage.token_pool),
    0tez,
    receiver)
  ], full_ps)
};
