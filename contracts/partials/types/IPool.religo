#include "./IAdmin.religo"
#if FA2_STANDARD_ENABLED
#include "./TypesFA2.religo"
#endif

let pool_func_count = 5n;
let admin_func_count = 3n;


type asset_amt_type = nat; // Type to represent amount of assets provided by user. Quipu Shares amount/FA1.2|FA2 tokens like Quipu shares/Tezos amount/Or some kind of internal share amount for XTZ-Token pools
type reward_amt_type = nat;
type clt_amt_type = nat;
type wtt_amt_type = nat;

type round_id_type = nat;
type user_id_type = nat;

type reward_type = [@layout:comb] {
  reward_amt : reward_amt_type
};

// Type to store WTT amt for user in current cycle, when Deposit or Withdraw happens
type cur_round_wtt_type = [@layout:comb] {
  total_wtt   : nat, // total amount of WTT produced by user
  wtt_amt     : nat, // amount of WTT minted by staked funds in indicated round
  round_id    : nat // round ID when WTT was updated last time(TODO: check if this is equal to last_date_provided round?)
};

type pool_account_type = [@layout:comb] {
  user_id               : nat,
  balance               : asset_amt_type, // balance abstraction(== token balance for only 1 token pools like Quipu Shares oriented pools, ==tezos balance for other types of pools)
  last_date_provided    : timestamp, // last date when fund's where provided
  last_date_withdrawn   : timestamp, // last date when fund's where withdrawn
  reward                : reward_type, // rewards
  wtt_balance           : cur_round_wtt_type,
  clt_balance           : nat, // balance of CLT token(pool token which paid for long holders, loyalty reward, slightly increases chance of win)
  clt_claimed           : nat, // Amount of claimed CLT
  last_clt_request      : timestamp, // last round when CLT balance was requested
#if FA2_STANDARD_ENABLED
  allowances            : set (address) // accounts allowed to act on behalf of the user
#else
  allowances            : map (address, nat) // pairs of the accounts and the amount of tokens they are approed to use
#endif
};

// type winners_bm_type = big_map(address, list(round_id_type));
type round_winners_type = big_map(round_id_type, address);

type pool_storage_type = [@layout:comb] {
#if FA2_STANDARD_ENABLED
  token_id              : token_id, // token identifier
#endif
  ledger                : big_map(address, pool_account_type), // Main ledger to store info about provided tokens
  id_to_addr            : big_map(nat, address), // Map user ID to user address
  round_winners         : round_winners_type,
  total_users           : nat,
  farm_contract         : address, // Farm contract address
  token_contract        : address, // Pool Token contract address
  prn_src               : address, // Random number source
  round_timeframe       : nat, // Round length in seconds
  round_id              : round_id_type, // Current round ID
  round_start           : timestamp, // Round start timestamp
  total_asset_pool      : asset_amt_type, // Total amount of provided assets. It shuold be equal to token_pool if ACCEPT_TOKENS_ENABLED, tezos_pool if ACCEPT_TEZOS_ENABLED or something like Shares supply for mixed pools
  tezos_pool            : asset_amt_type, // Total amount of XTZ provided if used
  token_pool            : asset_amt_type, // Total amount of Token provided if used
  last_date_provided    : timestamp, // Last time when total amt was increased
  last_date_withdrawn   : timestamp, // Last time when total amt was decreased
  round_acc_wtt         : nat, // accumulated amount of WTT during current round(calculated on deposit or withdraw)
  total_wtt             : asset_amt_type, // total amount of WTT tokens for all time( += round_acc_wtt every new round)
  total_clt             : asset_amt_type, // total amount of CLT tokens
  max_clt               : asset_amt_type, // max CLT to be minted
  next_user_id          : nat, // Next ID to user for new user
  reward_rec_contract   : address, // Contract that receives 40% of total round reward
  before_claim          : reward_amt_type, // Contract balance of reward coin(xtz or some token) before claim
  last_reward_amt       : reward_amt_type
};

#if ACCEPT_TEZOS_ENABLED
type enter_pool_params = unit;
#else
type enter_pool_params = asset_amt_type;
#endif
#if ACCEPT_TEZTOK_ENABLED
type exit_pool_params = (asset_amt_type, asset_amt_type, asset_amt_type);
#else
type exit_pool_params = asset_amt_type;
#endif

type input_assets_type =
| InTezos                 (enter_pool_params)
| InToken                 (enter_pool_params)
| InTezTok                (enter_pool_params) ;

type output_asset_type =
| OutTezos                (exit_pool_params)
| OutToken                (exit_pool_params)
| OutTezTok               (exit_pool_params) ;

#if OFFCHAIN_DRAW
type draw_params = (nat, nat, nat); // winner ID, random seed for ID search, same for PROBABILITY
#else
type draw_params = nat;
#endif

type start_pool_params = asset_amt_type;
type withdraw_reward_params = nat;
type new_round_params = unit; // Winner ID

type pool_action = 
| StartPool (start_pool_params) // start pool
| Round  // start new round
| EnterPool (enter_pool_params) // invest funds to common pool
| ExitPool (exit_pool_params) // withdraw fund's from pool
| Draw (draw_params); // entrypoint to accept RND number and make draw
// | WithdrawReward (withdraw_reward_params) ; // withdraw reward after win

type default_params = unit;
type use_params = pool_action;
type get_reserves_params = contract((nat, nat));

#if FA2_STANDARD_ENABLED
#include "../ActionFA2.religo"
#else
#include "../ActionFA12.religo"
#endif;

type return_type = (list(operation), pool_storage_type);
type pool_func = (pool_action, pool_storage_type, address) => return_type;
type token_func = (token_action, pool_storage_type, address) => return_type;
type admin_func = (pool_admin_action, pool_storage_type, address) => return_type;

type full_pool_storage_type = [@layout:comb] {
  storage               : pool_storage_type,
  admin_storage         : admin_storage_type,
  lambdas               : big_map(nat, bytes),
  token_lambdas_b       : big_map(nat, bytes),
  // pool_lambdas          : big_map(nat, pool_func),  map with pool-related functions code
  // token_lambdas         : big_map(nat, token_func),  map with token-related functions code
  // admin_lambdas         : big_map(nat, admin_func),  map with admin-related functions code
  metadata              : big_map(string, bytes) // metadata storage according to TZIP-016
};

type get_winner_opts_type = [@layout:comb] {
  initial_id : nat
}

type full_return_type = (list (operation), full_pool_storage_type);
let noOps = ([] : list (operation));

let max_clt_supply = 10_000_000_000n;
