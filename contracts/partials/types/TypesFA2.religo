type token_id = nat
type account =
[@layout:comb]
{
  balance         : nat,
  allowances      : set (address)
}

type token_metadata_info =
[@layout:comb]
{
  token_id  : token_id,
  extras    : map (string, bytes)
}

let default_token_id : token_id = 0n;

type fa2_storage_type =
[@layout:comb]
{
  total_supply              : nat,
  ledger                    : big_map (address, account),
  token_metadata            : big_map (token_id, token_metadata_info),
  metadata                  : big_map (string, bytes)
}

type fa2_return_type = (list(operation), fa2_storage_type);

type transfer_destination =
[@layout:comb]
{
  to_       : address,
  token_id  : token_id,
  amount    : nat
}

type transfer_param =
[@layout:comb]
{
  from_   : address,
  txs     : list(transfer_destination)
}

type balance_of_request =
[@layout:comb]
{
  owner       : address,
  token_id    : token_id
}

type balance_of_response =
[@layout:comb]
{
  request     : balance_of_request,
  balance     : nat
}

type balance_params =
[@layout:comb]
{
  requests    : list(balance_of_request),
  callback    : contract(list(balance_of_response))
}

type operator_param =
[@layout:comb]
{
  owner     : address,
  operator  : address,
  token_id  : token_id
}

type update_operator_param =
[@layout:comb]
| Add_operator (operator_param)
| Remove_operator (operator_param) ;

type transfer_params = list(transfer_param);
// type balance_params = michelson_pair_right_comb(balance_params_r)
type update_operator_params = list(update_operator_param);
