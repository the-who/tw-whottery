#include "./IPool.religo"

#if FA2_STANDARD_ENABLED
#include "./TypesFA2.religo"
#else
#include "./TypesFA12.religo"
#endif

// Factory storage types

#if FA2_STANDARD_ENABLED
let token_func_count = 3n;
type token_identifier = (address, nat);
#else
let token_func_count = 5n;
type token_identifier = address;
#endif

type factory_storage_type = 
[@layout:comb]
{
  counter           : nat,
  token_list        : big_map(nat, token_identifier),
  token_to_pool     : big_map(token_identifier, address),
  lambdas           : big_map(nat, bytes),
  token_lambdas_b   : big_map(nat, bytes),
  // pool_lambdas      : big_map(nat, pool_func),
  // token_lambdas     : big_map(nat, token_func),
  // admin_lambdas     : big_map(nat, admin_func),
  admin_storage     : admin_storage_type,
  metadata          : big_map(string, bytes)
};

type create_pool_func = (option(key_hash), tez, full_pool_storage_type) => (operation, address);

type factory_return_type = (list(operation), factory_storage_type);

type full_factory_return_type = (list(operation), factory_storage_type);

type transfer_type = TransferType (transfer_params);


type launch_params_type = 
[@layout:comb]
{
  token          : token_identifier,
  token_amount   : nat, // 0n should be sent at this moment. Pool will be filled later
  farm           : address,
  reward_contract: address,
  prn_src        : address,
  timeframe      : option (nat)
};

// Set pool func type
type set_pf_params_type = 
[@layout:comb]
{
  func : bytes,
  index : nat
};
// Set token func type
type set_tf_params_type = 
[@layout:comb]
{
  func : bytes,
  index : nat
};
// Set admin func type
type set_admf_params_type = 
[@layout:comb]
{
  func : bytes,
  index : nat
};

type factory_action = 
| LaunchPool (launch_params_type)
| SetPoolFunction (set_pf_params_type)
| SetTokenFunction (set_tf_params_type)
| SetAdminFunction (set_admf_params_type);

let accurancy_multiplier : nat = 1000000000000000n; // used to improve calculations accuracy
