type asset_amt_t        = nat;

type token_id_t = nat;


type tez_t              = unit

type fa12_token_t       = address

type fa2_token_t        = [@layout:comb] {
  address                 : address,
  id                      : nat
};

type token_t            =
| Tez                     (tez_t)
| Fa12                    (fa12_token_t)
| Fa2                     (fa2_token_t) ;

type balance_id_t = (address, token_t);

type token_amount_t = (token_t, asset_amt_t);
type transfer_destination_fa2_type =
[@layout:comb]
{
  to_       : address,
  token_id  : token_id_t,
  amount    : nat
}

type transfer_fa2_param_t =
[@layout:comb]
{
  from_   : address,
  txs     : list(transfer_destination_fa2_type)
};
type transfer_fa2_params_t = list(transfer_fa2_param_t);

type transfer_fa2_type  = transfer_fa2_params_t;
type transfer_fa12_type = michelson_pair(address, "from", michelson_pair(address, "to", nat, "value"), "");
type entry_fa12_type    = | TransferTypeFA12 (transfer_fa12_type);
type entry_fa2_type     = | TransferTypeFA2  (transfer_fa2_type);


module RewarderTypes = {
  type withdraw_params_t = [@layout:comb] {
    amount   : token_amount_t,
    receiver : option(address)
  };

  type deposit_params_t = [@layout:comb] {
    reward      : token_amount_t,
    pool_reward : asset_amt_t,
    user        : address
  };
}

module PoolsRouterTypes = {
  type withdraw_params_t = [@layout:comb] {
    amount   : token_amount_t,
    receiver : option(address)
  };

  type deposit_params_t = [@layout:comb] {
    amount      : token_amount_t,
    user        : address
  };
}