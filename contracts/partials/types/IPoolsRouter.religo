type stake_amt_t = [@layout:comb] {
  address: address,
  amount: asset_amt_t
};

type router_account_t = [@layout:comb] {
  balance : asset_amt_t,
  allowed_pair_tokens : set (token_id_t),
  borrowers : list (stake_amt_t),
  loaners : list(stake_amt_t)
};

type actions =
| Deposit (PoolsRouterTypes.deposit_params_t)
| Withdraw (PoolsRouterTypes.withdraw_params_t) 
| SetDelegate (key_hash) ;


type storage_t = [@layout:comb] {
  admin                   : address,
  tokens                  : big_map(token_id_t, token_t),
  ledger                  : big_map(token_id_t, router_account_t)
};

type return_t = (list(operation), storage_t);
