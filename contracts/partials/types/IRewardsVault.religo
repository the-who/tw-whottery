// #include "./TypesFA2.religo"



type account_t = [@layout:comb] {
  balance: asset_amt_t,
  allowances: set(address)
};

// type transfer_fa2_type  = list (transfer_param)
// type transfer_fa12_type = michelson_pair(address, "from", michelson_pair(address, "to", nat, "value"), "")
// type entry_fa12_type    = | TransferTypeFA12 (transfer_fa12_type)
// type entry_fa2_type     = | TransferTypeFA2  (transfer_fa2_type)




type actions =
// Pool action
| DepositReward (RewarderTypes.deposit_params_t)
// Common action(user can call this action)
| WithdrawReward (RewarderTypes.withdraw_params_t) 
| SetDelegate (key_hash) ;


type storage_t = [@layout:comb] {
  admin                   : address,
  tokens                  : big_map(token_id_t, token_t),
  pool_reward             : big_map(token_t, asset_amt_t),
  user_reward             : big_map(balance_id_t, asset_amt_t)
};

type return_t = (list(operation), storage_t);
