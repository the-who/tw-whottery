type trusted = address
type amt = nat

type account_type =
[@layout:comb]
{
  balance         : amt,
  allowances      : map(trusted, amt)
}

// contract storage
type storage_type =
[@layout:comb]
{
  total_supply      : amt,
  ledger            : big_map(address, account_type)
}

// define return for readability
type fa12_return_type = (list(operation), storage_type)

// define noop for readability
let noOps : list (operation) = [];

// Inputs
type transferParams = michelson_pair(address, "from", michelson_pair(address, "to", amt, "value"), "");
type approveParams = michelson_pair(trusted, "spender", amt, "value");
type balanceParams = michelson_pair(address, "owner", contract(amt), "");
type allowanceParams = michelson_pair(michelson_pair(address, "owner", trusted, "spender"), "", contract(amt), "");
type totalSupplyParams = (unit, contract(amt));

// Valid entry points
type tokenAction =
| Transfer (transferParams)
| Approve (approveParams)
| GetBalance (balanceParams)
| GetAllowance (allowanceParams)
| GetTotalSupply (totalSupplyParams) ;
