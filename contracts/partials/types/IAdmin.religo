type admin_storage_type =
[@layout:comb]
{
  admin           : address,
  paused          : bool,
  pending_admin   : option(address)
}

type admin_actions =
| SetPaused (bool)
| SetAdmin (address)
| ConfirmAdmin ;

// type set_timeframe_params_type = (nat, big_map(address, pool_account_type));

type pool_admin_action =
| SetPrnSrc (address)
| SetRoundTimeframe (nat)
| SetRewardContract (address) ;
// | WithdrawAll - when u ready to run

type full_admin_action =
| CallAdmin (admin_actions)
| CallPoolAdmin (pool_admin_action) ;

type admin_params_type = (admin_actions, admin_storage_type);