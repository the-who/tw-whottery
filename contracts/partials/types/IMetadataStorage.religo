// contract storage
type update_owner_type = {
  add : bool, // whether add or remove account from owners
  owner : address // account
};

type metadata_type = big_map (string, bytes)

type storage = {
  metadata : metadata_type, // metadata accoding to TZIP-16
  owners : set(address) // all owners
};

type return = (list (operation), storage)

// Valid entry points
type storage_action =
| Update_owners     (update_owner_type) // manage owner permissions
| Update_storage    (metadata_type) // update storage
| Get_metadata      (contract (metadata_type)) ; // send the storage data to the account