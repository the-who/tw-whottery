// Main function parameter types specific for FA1.2 standard
type transfer_params = michelson_pair(address, "from", michelson_pair(address, "to", nat, "value"), "");
type approve_params = michelson_pair(address, "spender", nat, "value");
type balance_params = michelson_pair(address, "owner", contract(nat), "");
type allowance_params = michelson_pair(michelson_pair(address, "owner", address, "spender"), "", contract(nat), "");
type total_supply_params = (unit, contract(nat));

type token_action =
| ITransfer             (transfer_params)
| IApprove              (approve_params)
| IGetBalance           (balance_params)
| IGetAllowance         (allowance_params)
| IGetTotalSupply       (total_supply_params) ;

type full_pool_action =
| Use                   (use_params)
| Default               (default_params)
| Admin                 (full_admin_action)
| Transfer              (transfer_params)
| Approve               (approve_params)
| GetBalance            (balance_params)
| GetAllowance          (allowance_params)
| GetTotalSupply        (total_supply_params)
| GetReserves           (get_reserves_params) ;
