// Helper function to get allowance for an account
let getAllowance = ((owner_account, spender, _ps) : (pool_account_type, address, pool_storage_type)) : nat => {
  switch(Map.find_opt(spender, owner_account.allowances)) {
    | Some (n) => n
    | None => 0n
  };
};

let checkAllowance = ((val1, val2) : (nat, nat)) => {
  if (val1 < val2) {
    (failwith("Pool/not-enough-allowance") : unit);
  } else {
    unit;
  };
};

let updateSender = ((value, addr, sender_account, pool_storage) : (nat, address, pool_account_type, pool_storage_type)) => {
  if (addr != Tezos.sender) {
    let spender_allowance = getAllowance(sender_account, Tezos.sender, pool_storage);
    let _t = checkAllowance(spender_allowance, value);
    let new_acc = {
      ...sender_account,
      allowances : Map.add(Tezos.sender, abs(spender_allowance - value), sender_account.allowances)
    };
    new_acc;
  } else {
    let new_sender_acc = {...sender_account, clt_balance : abs(sender_account.clt_balance - value) };
    new_sender_acc;
  };
};

let checkSelfTransfer = ((a1, a2) : (address, address)) => {
  if (a1 == a2) {
    (failwith("Pool/self-transfer") : unit);
  } else {
    unit;
  };
}

let checkBalance = ((acc_balance, value) : (nat, nat)) => {
  if (acc_balance < value) {
    (failwith("Pool/not-enough-balance") : unit);
  } else {
    unit;
  };
}

// Transfer token to another account
let transfer = ((p, pool_storage, _this) : (token_action, pool_storage_type, address)) : return_type => {
  let new_pool_storage = switch(p) {
    | ITransfer (params) => {
      // let new_pool_st_aft_upd_rew = update_reward(pool_storage);

      let value : nat = params[1][1];
      checkSelfTransfer(params[0], params[1][0]);
      let sender_account : pool_account_type = getAccount(params[0], pool_storage);
      checkBalance(sender_account.clt_balance, value);
      let new_sender_acc = updateSender(value, params[0], sender_account, pool_storage);
      let new_st_aft_sender_rew_upd = updateUserReward(pool_storage);
      let new_st_aft_sender_upd = { ...new_st_aft_sender_rew_upd, ledger : Big_map.add(params[0], new_sender_acc, new_st_aft_sender_rew_upd.ledger) };

      let dest_account : pool_account_type = getAccount(params[1][0], new_st_aft_sender_upd);

      let new_dest_account = { ...dest_account, clt_balance : dest_account.clt_balance + value };
      let new_st_aft_dest_upd = { ...new_st_aft_sender_upd, ledger : Big_map.add(params[1][0], new_dest_account, new_st_aft_sender_upd.ledger) };
      new_st_aft_dest_upd;
    }
    | IApprove(_params) => pool_storage
    | IGetBalance(_params) => pool_storage
    | IGetAllowance(_params) => pool_storage
    | IGetTotalSupply(_params) => pool_storage
  };
  (noOps, new_pool_storage);
};

// Approve an nat to be spent by another address in the name of the sender
let approve = ((p, pool_storage, _this) : (token_action, pool_storage_type, address)) : return_type => {
  let new_pool_storage = switch(p) {
    | ITransfer(_params) => pool_storage
    | IApprove(params) => {
      if (params[0] == Tezos.sender) {
        failwith("Pool/self-approval");
      } else {
        unit;
      };
      let sender_account = getAccount(Tezos.sender, pool_storage);
      let spender_allowance : nat = getAllowance(sender_account, params[0], pool_storage);
      if (spender_allowance > 0n && params[1] > 0n) {
        failwith("UnsafeAllowanceChange");
      } else {
        unit;
      };
      // Update sender account
      let new_sender_acc = { ...sender_account, allowances : Map.add(params[0], params[1], sender_account.allowances) };
      // Update storage
      let new_st_aft_sender_allowance_upd = { ...pool_storage, ledger : Big_map.add(Tezos.sender, new_sender_acc, pool_storage.ledger) };
      new_st_aft_sender_allowance_upd;
    }
    | IGetBalance(_params) => pool_storage
    | IGetAllowance(_params) => pool_storage
    | IGetTotalSupply(_params) => pool_storage
  };
  (noOps, new_pool_storage);
};

// View function that forwards the balance of source to a contract
let get_balance = ((p, ps, _this) : (token_action, pool_storage_type, address)) : return_type => {
  let def_ret = (noOps, ps);
  let ret = switch(p) {
    | ITransfer(_params) => def_ret
    | IApprove(_params) => def_ret
    | IGetBalance(params) => {
      let owner_account = getAccount(params[0], ps);
      let operations = [Tezos.transaction(owner_account.clt_balance, 0mutez, params[1])];
      (operations, ps);
    }
    | IGetAllowance(_params) => def_ret
    | IGetTotalSupply(_params) => def_ret
  };
  ret;
};

// View function that forwards the total_supply to a contract
let get_total_supply = ((p, ps, _this) : (token_action, pool_storage_type, address)) : return_type => {
  let def_ret = (noOps, ps);
  let ret = switch(p) {
    | ITransfer(_params) => def_ret
    | IApprove(_params) => def_ret
    | IGetBalance(_params) => def_ret
    | IGetAllowance(_params) => def_ret
    | IGetTotalSupply(params) => ([Tezos.transaction(ps.total_clt, 0mutez, params[1])], ps);
  };
  ret;
};
// View function that forwards the allowance amt of spender in the name of tokenOwner to a contract
let get_allowance_to_contract = ((p, ps, _this) : (token_action, pool_storage_type, address)) : return_type => {
  let def_ret = (noOps, ps);
  let ret = switch(p) {
    | ITransfer(_params) => def_ret
    | IApprove(_params) => def_ret
    | IGetBalance(_params) => def_ret
    | IGetAllowance(params) => {
      let owner_account = getAccount(params[0][0], ps);
      let spender_allowance : nat = getAllowance(owner_account, params[0][1], ps);
      ([Tezos.transaction(spender_allowance, 0mutez, params[1])], ps);
    }
    | IGetTotalSupply(_params) => def_ret;
  };
  ret;
};
