// Helper function to get allowance for an account
let getAllowance = ( (owner_account, spender, _pool_storage) : (pool_account_type, address, pool_storage_type)) : bool => {
  Set.mem(spender, owner_account.allowances)
};

// Perform transfers from one owner
let iterate_transfer = ((ps, user_trx_params) : (pool_storage_type, transfer_param)) : pool_storage_type => {
  // Perform single transfer
  let make_transfer = ((ps, transfer) : (pool_storage_type, transfer_destination)) : pool_storage_type => {
    // Retrieve sender account from storage
    let sender_account : pool_account_type = getAccount(user_trx_params.from_, ps);

    // Check permissions
    if ((user_trx_params.from_ == Tezos.sender) || (getAllowance(sender_account, Tezos.sender, ps))) {
      unit;
    } else {
      (failwith("FA2_NOT_OPERATOR") : unit);
    };
    
    // Token id check
    if (default_token_id != transfer.token_id) {
      (failwith("FA2_TOKEN_UNDEFINED") : unit);
    } else {
      unit;
    };
    
    // Balance check
    if (sender_account.clt_balance < transfer.amount) {
      (failwith("FA2_INSUFFICIENT_BALANCE") : unit);
    } else {
      unit;
    };

    let new_pool_st_aft_sender_rew = updateUserReward(ps);

    // Update sender balance
    let new_sender_acc = { ...sender_account, clt_balance : abs(sender_account.clt_balance - transfer.amount) };
    
    // Update storage
    let new_pool_st_aft_sender_upd = { ...new_pool_st_aft_sender_rew , ledger : Big_map.update(user_trx_params.from_, Some (new_sender_acc), new_pool_st_aft_sender_rew.ledger) };
    
    // Create or get destination account
    let dest_account : pool_account_type = getAccount(transfer.to_, ps);
    let new_pool_st_aft_dest_rew = updateUserReward(new_pool_st_aft_sender_upd);
    
    // Update destination balance
    let new_dest_acc = { ...dest_account, clt_balance : dest_account.clt_balance + transfer.amount, last_clt_request : Tezos.now };
    
    // Update storage
    let new_pool_st_aft_dest_upd = { ...new_pool_st_aft_dest_rew , ledger : Big_map.update(transfer.to_, Some (new_dest_acc), new_pool_st_aft_dest_rew.ledger) };
    new_pool_st_aft_dest_upd;
  };
  (List.fold (make_transfer, user_trx_params.txs, ps));
};

let checkTokenId = ((token_id) : (nat)) => {
  if (default_token_id != token_id) {
    failwith("FA2_TOKEN_UNDEFINED");
  } else {
    unit;
  };
};

let checkOwner = ((owner) : (address)) => {
  if (Tezos.sender != owner) {
    failwith("FA2_NOT_OWNER");
  } else {
    unit;
  };
};

// Perform single operator update
let iterate_update_operator = ((ps, params) : (pool_storage_type, update_operator_param)) : pool_storage_type => {
  let p = switch(params) {
    | Add_operator (n) => n
    | Remove_operator (n) => n
  };
  // Token id check 
  checkTokenId(p.token_id);
  // Check an owner
  checkOwner(p.owner);
  // Create or get sender account
  let sender_account = getAccount(p.owner, ps);
  let new_acc = switch(params) {
    | Add_operator(param) => {
      // Set operator
      let new_sender_acc = { ...sender_account, allowances : Set.add(param.operator, sender_account.allowances) };
      new_sender_acc;
    }
    | Remove_operator(param) => {
      // Set operator
      let new_sender_acc = { ...sender_account, allowances : Set.remove(param.operator, sender_account.allowances) };
      new_sender_acc;
    }
  };
  let new_ledger = Big_map.update(p.owner, Some (new_acc), ps.ledger);
  let new_pool_storage = { ...ps, ledger : new_ledger };
  new_pool_storage;
};

let transfer = ((p, pool_storage, _this) : (token_action, pool_storage_type, address)) : return_type => {
  let new_pool_storage = switch(p) {
    | ITransfer(params) => {
      let new_pool_st_aft_upd_rew = updateUserReward(pool_storage);
      let new_pool_st_aft_transfer = List.fold(iterate_transfer, params, new_pool_st_aft_upd_rew);
      new_pool_st_aft_transfer;
    }
    | IBalance_of(_params) => pool_storage
    | IUpdate_operators(_params) => pool_storage
  };
  (noOps, new_pool_storage);
};

let get_balance_of = ((p, pool_storage, _this) : (token_action, pool_storage_type, address)) : return_type => {
  let el = ([] : list (balance_of_response));
  let cb = switch(p) {
    | IBalance_of(p) => p.callback
    | ITransfer (_p) => (failwith("Pool/Wrong-param") : contract (list (balance_of_response)))
    | IUpdate_operators (_p) => (failwith("Pool/Wrong-param") : contract (list (balance_of_response)))
  };
  let new_response = switch(p) {
    | ITransfer(_params) => el
    | IBalance_of(balance_params) => {
      // Perform single balance lookup
      let look_up_balance = ((l, request) : (list (balance_of_response), balance_of_request)) : list (balance_of_response) => {
        checkTokenId(request.token_id);
        // Retrieve the asked account balance from storage
        let sender_account = getAccount(request.owner, pool_storage);

        // Form the response
        let response : balance_of_response = {
          request   : request,
          balance   : sender_account.clt_balance
        };
        [response, ...l];
      };

      // Collect balances info
      let accumulated_response : list (balance_of_response) = List.fold(look_up_balance, balance_params.requests, ([] : list(balance_of_response)));
      accumulated_response;
    }
    | IUpdate_operators(_params) => el
  };
  let op = Tezos.transaction(new_response, 0mutez, cb);
  ([op], pool_storage);
};

let update_operators = ((p, pool_storage, _this) : (token_action, pool_storage_type, address)) : return_type => {
  let new_pool_storage = switch(p) {
    | ITransfer(_params) => pool_storage
    | IBalance_of(_params) => pool_storage
    | IUpdate_operators(params) => {
      List.fold(iterate_update_operator, params, pool_storage);
    }
  };
  (noOps, new_pool_storage);
};
