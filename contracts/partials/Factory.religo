#include "./types/IFactory.religo"
#include "./common/Admin.religo"
#include "./common/Common.religo"
#include "./MethodPool.religo"
#include "./CreatePool.religo"

// Create(initialize) pool contract for quipu shares, token's etc.
let launchPool = ((params, fs) : (launch_params_type, factory_storage_type)) : full_factory_return_type => {
  // check requirements
  checkReqs(params.token, fs);

  let new_factory_storage = {
    ...fs,
    token_list : Big_map.add(fs.counter, params.token, fs.token_list),
    counter    : fs.counter + 1n
  };

  // prepare storage traking into account the token standard
  let pool_storage = {
#if FA2_STANDARD_ENABLED
    token_id            : params.token[1],
    token_contract      : params.token[0],
#else
    token_contract      : params.token,
#endif
    ledger              : (Big_map.empty : big_map(address, pool_account_type)),
    id_to_addr          : (Big_map.empty : big_map(nat, address)),
    round_winners       : (Big_map.empty : big_map(round_id_type, address)),
    total_users         : 0n,
    farm_contract       : params.farm,
    prn_src             : params.prn_src,
    round_timeframe     : unoptOr(params.timeframe, 604800n),
    round_id            : 0n,
    round_start         : Tezos.now,
    total_asset_pool    : 0n,
    tezos_pool          : Tezos.amount / 1mutez, // 0mutez should be sent at this moment. Pool will be filled later
    token_pool          : params.token_amount, // 0n should be sent at this moment. Pool will be filled later
    last_date_provided  : Tezos.now,
    last_date_withdrawn : Tezos.now,
    round_acc_wtt       : 0n,
    total_wtt           : 0n,
    total_clt           : 0n,
    max_clt             : max_clt_supply,
    next_user_id        : 1n,
    reward_rec_contract : params.reward_contract,
    before_claim        : 0n,
    last_reward_amt     : 0n
  };
  // prepare the operation to originate the Pair contract; note: the XTZ for initial liquidity are sent
  let res = createPool((None (unit) : option(key_hash)), Tezos.amount, {
    admin_storage   : fs.admin_storage,
    metadata        : fs.metadata,
    // pool_lambdas  : fs.pool_lambdas,
    storage         : pool_storage,
    token_lambdas_b : fs.token_lambdas_b,
    lambdas         : fs.lambdas
  });
  let new_token_to_pool = Big_map.add(params.token, res[1], new_factory_storage.token_to_pool);
  let new_factory_storage_after_create = { ...new_factory_storage, token_to_pool : new_token_to_pool };
  ([res[0]], new_factory_storage_after_create);
};

let setLambda = ((idx, b, lambdas) : (nat, bytes, big_map(nat, bytes))) : big_map(nat, bytes) => {
  switch(Big_map.find_opt(idx, lambdas)) {
    | Some (_) => (failwith(FactoryCore.func_already_set) : big_map(nat, bytes))
    | None => Big_map.add(idx, b, lambdas)
  };
};

// Set the dex function code to factory storage
let setPoolFunction = ((idx, f, fs) : (nat, bytes, factory_storage_type)) : factory_storage_type => {
  let new_lambdas = setLambda(idx, f, fs.lambdas);
  let new_factory_storage = { ...fs, lambdas : new_lambdas };
  new_factory_storage;
};

// Set the token function code to factory storage
let setTokenFunction = ((idx, f, fs) : (nat, bytes, factory_storage_type)) : factory_storage_type => {
  let new_token_lambdas = setLambda(idx, f, fs.token_lambdas_b);
  let new_factory_storage = { ...fs, token_lambdas_b : new_token_lambdas };
  new_factory_storage;
};

let setAdminFunction = ((idx, f, fs) : (nat, bytes, factory_storage_type)) : factory_storage_type => {
  let new_lambdas = setLambda(idx, f, fs.lambdas);
  let new_fs = { ...fs, lambdas : new_lambdas };
  new_fs;
};

let checkIndex = ((i1, i2) : (nat, nat)) : unit => {
  if (i1 > i2) {
    (failwith("Factory/wrong-index") : unit);
  } else {
    unit;
  };
};

// Factory - Singleton used to deploy new pool pair that invests tokens
let main = ((p, fs) : (factory_action, factory_storage_type)) : full_factory_return_type => {
  isAdmin(fs.admin_storage);
  switch(p) {
    | LaunchPool (params)        => {
      launchPool(params, fs);
    }
    | SetPoolFunction (params)   => {
      checkIndex(params.index, pool_func_count);
      let new_storage = setPoolFunction(params.index, params.func, fs);
      (noOps, new_storage);
    }
    | SetTokenFunction (params)  => {
      checkIndex(params.index, token_func_count);
      let new_storage = setTokenFunction(params.index, params.func, fs);
      (noOps, new_storage);
    }
    | SetAdminFunction (params)  => {
      // checkIndex(params.index, admin_func_count);
      let new_storage = setAdminFunction(params.index, params.func, fs);
      (noOps, new_storage);
    }
  };
};