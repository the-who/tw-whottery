let default_return = (ps : pool_storage_type) : return_type => (noOps, ps);

let startPool = ((p, ps, _this) : (pool_action, pool_storage_type, address)) : return_type => {
  switch(p) {
    | StartPool (token_amt)  => {
      checkStartPreconditions(token_amt, ps);
      let tezos_amt = Tezos.amount / 1mutez;
      let creator_acc = getCreatorAccount(token_amt, ps);
      // Update ledger
      let new_ledger = Big_map.literal([
        (Tezos.sender, creator_acc)
      ]);
      // Update ID => user address map
      let new_id_to_addr = Big_map.literal([
        (ps.next_user_id, Tezos.sender)
      ]);
      let next_user_id = ps.next_user_id + 1n; // increment ID
      let new_round_id = 1n; // Init round ID

      /*
        Prepare operations. If we work with Quipu shares and claiming rewards on Quipu:
        - Transfer tokens from user's address to contract
      */
      let ops = acceptFundsAndInvest(token_amt, ps);
      
      let new_ps = {
        ...ps,
        ledger : new_ledger,
        id_to_addr : new_id_to_addr,
        total_users : ps.total_users + 1n,
        round_id : new_round_id,
        round_start : Tezos.now,
        total_asset_pool : token_amt, // TODO
        tezos_pool : tezos_amt,
        token_pool : token_amt,
        last_date_provided : Tezos.now,
        next_user_id : next_user_id
      };
      (ops, new_ps);
    }
    | _ => default_return(ps)
  };
}

/*
  Write user rewards, withdraw pool reward, start new round
  - calculate user and pool reward amount
  - update user account (reward amt and winned rounds set)
  - withdraw pool reward on special reward contract
*/
let round = ((p, ps, _this) : (pool_action, pool_storage_type, address)) : return_type => {
  checkSelfCall(); // This EP could be called only by the contract
  switch(p) {
    | Round => {
      let winner_addr = unoptOr(Big_map.find_opt(ps.round_id, ps.round_winners), getAddrById(1n, ps)); // Send funds to admin account in case of missed winner id(this situation should not happen or admin id should be selected by common rules, admin == address who initiated pool)
      let acc = getAccount(winner_addr, ps);
      // Calculate user reward
      let total_reward_amt = abs (getCurrentRewardCoinBalance() - ps.before_claim);
      let (user_reward_amt, pool_reward_amt) = getUserReward(total_reward_amt);
      // Update user reward and winned rounds set
      let new_acc = {
        ...acc,
        reward : { reward_amt : user_reward_amt }
      };
      let withdraw_pool_reward_op = withdrawPoolReward(total_reward_amt, pool_reward_amt, winner_addr, ps);

      let new_ps = {
        ...ps,
        ledger : Big_map.update(getAddrById(new_acc.user_id, ps), Some (new_acc), ps.ledger),
        round_id : ps.round_id + 1n,
        round_start : Tezos.now,
        last_reward_amt : total_reward_amt
      };
      let ops = [withdraw_pool_reward_op];
      (ops, new_ps);
    }
    | _ => default_return(ps)
  };
}

let enterPool = ((p, ps, _this) : (pool_action, pool_storage_type, address)) : return_type => {
  switch(p) {
    | EnterPool (params)         => {
      // TODO: check time before next round start, if it is < than some amount then last_provided = next_round_start_timestamp
      checkTokensAmt(params);
      onlyToken();
      
      let user = Tezos.sender;
      let acc = getAccount(user, ps);
      let (new_acc, income, out_v) = addAssets(params, acc, ps);
      // Add new participants in two maps(address => account, ID => address)
      let (new_ledger, new_id_to_addr, new_next_user_id, new_total_users) = switch(Big_map.find_opt(Tezos.sender, ps.ledger)) {
        | Some (_n) => {
          let new_l = Big_map.update(user, Some (new_acc), ps.ledger);
          (new_l, ps.id_to_addr, ps.next_user_id, ps.total_users);
        }
        | None => {
          let new_l = Big_map.add(user, new_acc, ps.ledger);
          // Add new users to id => address map to find by ID later
          let new_id_to_addr = Big_map.add(new_acc.user_id, user, ps.id_to_addr);
          (new_l, new_id_to_addr, ps.next_user_id + 1n, ps.total_users + 1n);
        }
      };
      let wtt_diff = abs (new_acc.wtt_balance.wtt_amt - acc.wtt_balance.wtt_amt);
      let ops = acceptFundsAndInvest(income, ps);
      let new_ps = {
        ...ps,
        ledger : new_ledger,
        id_to_addr : new_id_to_addr,
        total_users : new_total_users,
        total_asset_pool : ps.total_asset_pool + income,
        tezos_pool : ps.tezos_pool + out_v[1],
        token_pool : ps.token_pool + out_v[0],
        round_acc_wtt : ps.round_acc_wtt + wtt_diff,
        total_wtt : ps.total_wtt + wtt_diff,
        last_date_provided : Tezos.now,
        next_user_id : new_next_user_id
      };

      (ops, new_ps);
    }
    | _ => default_return(ps)
  };
};

// 
let exitPool = ((p, ps, _this) : (pool_action, pool_storage_type, address)) : return_type => {
  switch(p) {
    | ExitPool (n)                   => {
      let _this = Tezos.self_address;
      let acc = getAccount(Tezos.sender, ps);
      let (new_acc, outcome, out_v) = subAssets(n, acc, ps);
      let (tezos_amt_to_w, token_amt_to_w) = out_v;
      let new_ledger = Big_map.update(Tezos.sender, Some (new_acc), ps.ledger);
      let wtt_diff = abs(acc.wtt_balance.wtt_amt - new_acc.wtt_balance.wtt_amt);
      let ops = divestFundsAndSend(n, ps);
      let new_ps = {
        ...ps,
        ledger                : new_ledger,
        tezos_pool            : abs(ps.tezos_pool - tezos_amt_to_w),
        token_pool            : abs(ps.token_pool - token_amt_to_w),
        total_asset_pool      : abs(ps.total_asset_pool - outcome),
        last_date_provided    : Tezos.now,
        last_date_withdrawn   : Tezos.now,
        round_acc_wtt         : abs (ps.round_acc_wtt - wtt_diff),
        total_wtt             : abs (ps.total_wtt - wtt_diff)
      };
      (ops, new_ps);
    }
    | _ => default_return(ps)
  };
};

/* 
  At this moment this EP must be called by rnd_src address(same as admin for tests)
  it is necessary to send some amount of XTZ to this EP to simulate reward distribution
  called by Pseudo Random Number source (prn_src address in pool storage)
  - find winner id
  - calls "claim" entrypoint on farm
  - remember current balance of coin which used for rewards distribution (XTZ for Quipu shares) to calculate received reward later
  - call newRound self entrypoint to calculate user/pool rewards
*/
let draw = ((p, ps, _this) : (pool_action, pool_storage_type, address)) : return_type => {

  isRndSrc(ps); // check tx source
  checkDrawTime(ps); // check if round ended

  switch(p) {
    | Draw (random)                  => {
      // There some space to experimets with on-chain draw's.
      // Now this entrypoint accept's pre-calculated winner ID(all calculatoins are off-chain)
      let _total_wtt_in_round = ps.round_acc_wtt;
      let _id_to_find = (ps.total_users mod random) + 1n; // Theoretical winner id
      // TODO: find probability from random number
      let _prob = (100n mod random) + 1n;
      // let winner_acc_id = getWinner(id_to_find, prob, { initial_id : id_to_find }, ps);
      let winner_acc_id = random; // TODO
      let acc = getAccountById(winner_acc_id, ps);
#if TEST_ENABLED
      let balance_before_claim = 0n;
#else
      let balance_before_claim = getCurrentRewardCoinBalance();
#endif
      // Pass winner ID to newRound self entrypoint
      let new_round_winners = updateWinners(acc, ps);

      // Claim reward
      let claim_op = claimReward(ps);
      // let new_round_ep : contract(unit) = Tezos.self("%round");
      /*
        next line didn't work for some reason
        let call_new_round_op = Tezos.transaction(unit, 0mutez, new_round_ep);
      */
      let ep = unopt((Tezos.get_entrypoint_opt("%round", Tezos.self_address) : option(contract(unit))), PoolCore.cant_get_round_ep);
      let call_new_round_op = Tezos.transaction(unit, 0mutez, ep);
      
      let new_ps = {
        ...ps,
        round_winners : new_round_winners,
        before_claim : balance_before_claim
      };

      let ops = [claim_op, call_new_round_op];
      (ops, new_ps);
    }
    | _ => default_return(ps)
  };
};
