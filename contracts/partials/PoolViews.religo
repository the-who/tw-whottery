// Get total provided assets amount in Pool
[@view] let total_asset_amt = ((_ , s): (unit , full_pool_storage_type)) : nat => s.storage.total_asset_pool;
// get WTT amount for current round
[@view] let get_wtt = ((addr, full_ps): (address, full_pool_storage_type)) : nat => {
  let acc = getAccount(addr, full_ps.storage);
  getUserWttForRound(acc, full_ps.storage);
};
// get total WTT amount for user
[@view] let get_total_wtt = ((addr, full_ps): (address, full_pool_storage_type)) : nat => {
  let acc = getAccount(addr, full_ps.storage);
  getUserTotalWtt(acc, full_ps.storage);
};

type last_draw_info = [@layout:comb] {
  starttime   : timestamp,
  endtime     : timestamp,
  winner      : address,
  reward      : asset_amt_type
};

[@view] let get_last_draw_info = ((_, full_ps) : (unit, full_pool_storage_type)) : last_draw_info => {
  let ps = full_ps.storage;
  let start : timestamp = ps.round_start - int(ps.round_timeframe);
  {
    starttime: start,
    endtime: ps.round_start,
    winner: unopt(Big_map.find_opt(ps.round_id, ps.round_winners), PoolCore.cant_find_winner),
    reward: ps.last_reward_amt
  };
};