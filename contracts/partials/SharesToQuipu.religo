/*
  Create operations specific for Quipu v3 shares
  - EnterPool = acceptFundsAndInvest
  - ExitPool = divestFundsAndSend
*/
#include "./types/ISharesToQuipu.religo"

let getTotalRewardAmt = ((ps) : (pool_storage_type)) => {
  let total_reward_amt = abs(Tezos.balance / 1mutez - ps.tezos_pool);
  total_reward_amt;
};

let getFarmClaimEp = ((farm_address) : (address)) : contract(claim_ep_param_type) => {
  switch (Tezos.get_entrypoint_opt("%withdrawProfit", farm_address) : option(contract(claim_ep_param_type))) {
    | Some (contr) => contr
    | None => (failwith("NO_QUIPU_CLAIM_EP") : contract(claim_ep_param_type))
  };
};

// Function should return balance_of token for pool contract or Tezos.balance if reward distributed in XTZ
let getCurrentRewardCoinBalance = () : reward_amt_type => {
  let rew = Tezos.balance / 1mutez;
  rew;
};

// Claim reward from farm
let claimReward = ((ps) : (pool_storage_type)) : operation => {
  let op = Tezos.transaction(Tezos.self_address, 0mutez, getFarmClaimEp(ps.farm_contract));
  op;
};

// Send pool reward to special contract. This function is not common since some farms can pay rewards not only in XTZ
let withdrawPoolReward = ((total_amt, pool_amt, winner, ps) : (nat, nat, address, pool_storage_type)) => {
  let amt = total_amt * 1mutez;
  // We need rewarder deposit type
  let rewarder_dep_params : RewarderTypes.deposit_params_t = {
    reward : (Tez, total_amt),
    pool_reward : pool_amt,
    user : winner
  };
  let rewarder_dep_ep = unopt(
    (Tezos.get_entrypoint_opt("%depositReward", ps.reward_rec_contract) : option(contract(RewarderTypes.deposit_params_t))),
    "Pool/Cant-get-snd-ctrt");
  let op = Tezos.transaction(rewarder_dep_params, amt, rewarder_dep_ep);
  op;
};

// prepare operations to get initial liquidity and invest it to farm, then revoke allowance
let acceptFundsAndInvest = ((token_amt, ps) : (asset_amt_type, pool_storage_type)) : list (operation) => {
  let user_addr = Tezos.sender; // user address
  let this = Tezos.self_address; // contract address
  let token_contr = ps.token_contract;
  let _farm_contr = ps.farm_contract;

  // Prepare operations to get initial liquidity and invest it to farm, then revoke allowance
  // Transfer tokens from user account to pool (this is necessary, because pool plays role of rewards collector)
  let user_to_cont_op = Tezos.transaction(
    wrapTransferTrx(user_addr, this, token_amt),
    0mutez,
    getTokenTransferEp(token_contr));

  // transfer Quipu shares from user to the contract
  let ops = [user_to_cont_op];
  ops;
};

let divestFundsAndSend = ((token_amt, ps) : (asset_amt_type, pool_storage_type)) : list (operation) => {
  let _user_addr = Tezos.sender;
  let this = Tezos.self_address;
  let _token_contr = ps.token_contract;
  let _farm_contr = ps.farm_contract;

  checkSharesAmt(token_amt);

  let withdraw_tokens_op = Tezos.transaction(wrapTransferTrx(this, Tezos.sender, token_amt), 0mutez, getTokenTransferEp(ps.token_contract));
  let ops = [withdraw_tokens_op];

  ops;
};