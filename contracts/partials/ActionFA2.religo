// Main function parameter types specific for FA2 standard
type transfer_params = list (transfer_param);
type update_operator_params = list (update_operator_param);

type token_action =
| ITransfer                (transfer_params)
| IBalance_of              (balance_params)
| IUpdate_operators        (update_operator_params) ;

type full_pool_action =
| Use                     (use_params)
| Default                 (default_params)
| Transfer                (transfer_params)
| Balance_of              (balance_params)
| Update_operators        (update_operator_params)
| Admin                   (full_admin_action)
| Get_reserves            (get_reserves_params) ;
