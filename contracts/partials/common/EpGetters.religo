// Helpers to get entrypoints

#if FA2_STANDARD_ENABLED
// Helper function to get token contract update_operators entrypoint
let getTokenUpdopsEp = ( (token_address) : (address)) : contract(update_operator_params) => {
  switch (Tezos.get_entrypoint_opt("%update_operators", token_address) : option(contract(update_operator_params))) {
    | Some (contr) => contr
    | None => (failwith("NO_TOKEN_CONTRACT") : contract(update_operator_params))
  };
};
#else
// Helper function to get token contract update_operators entrypoint
let getTokenApproveEp = ((token_address) : (address)) : contract(approveParams) => {
  switch (Tezos.get_entrypoint_opt("%approve", token_address) : option(contract(approveParams))) {
    | Some (contr) => contr
    | None => (failwith("NO_TOKEN_CONTRACT") : contract(approveParams))
  };
};
#endif

// Helper function to get token contract transfer entrypoint
let getTokenTransferEp = ( (token_address) : (address)) : contract(transfer_type) => {
  switch (Tezos.get_entrypoint_opt("%transfer", token_address) : option(contract(transfer_type))) {
    | Some (contr) => contr
    | None => (failwith("NO_TOKEN_CONTRACT") : contract(transfer_type))
  };
};
