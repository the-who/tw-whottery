#include "./Errors.religo"
#include "./CommonHelpers.religo"
#include "./PoolHelpers.religo"
#include "./Checkers.religo"
#include "./EpGetters.religo"

// Helper function to prepare the token transfer
let wrapTransferTrx = ((owner, receiver, value) : ( address, address, nat)) : transfer_type => {
#if FA2_STANDARD_ENABLED
  TransferType ([
    {
      from_ : owner,
      txs : [{
          to_ : receiver,
          token_id : default_token_id,
          amount : value
        }]
    }
  ]);
#else
  TransferType (owner, (receiver, value));
#endif
};

let userAccumulatedWttForRounds = ((acc, ps) : (pool_account_type, pool_storage_type)) : (wtt_amt_type, nat) => {
  let round_start = ps.round_start;
  let last_change = getLatest(round_start, acc.last_date_provided);
  let _staked_in_round = abs (Tezos.now - last_change);
  let time_staked = abs (Tezos.now - acc.last_date_provided);
  let new_user_wtt_amt = acc.balance * time_staked;
  // (Amount of WTT, number of rounds that this amount staked)
  (new_user_wtt_amt, time_staked / ps.round_timeframe);
};

#if FA2_STANDARD_ENABLED
let wrapUpdopsTrx = ((sndr, oper) : (address, address)) => {
  let upd_op_param = {
    owner: sndr,
    operator: oper,
    token_id: default_token_id
  };
  upd_op_param;
};
#endif

#if CLAIM_QUIPU_ENABLED
#include "../SharesToQuipu.religo"
#endif


#if FA2_STANDARD_ENABLED
#include "../MethodFA2.religo"
#else
#include "../MethodFA12.religo"
#endif

/*
There was research to make On-chain draw(selection of winner ID), but I didn't managed to do this without iterating big_map or without storing a lot of additional info
So for now contract wait's only for winner ID
let rec getWinner = ((id_to_find, prob, opts, ps) : (nat, nat, get_winner_opts_type, pool_storage_type)) : nat => {
  if (id_to_find == 0n || id_to_find > abs(ps.next_user_id - 1n)) {
    failwith("Pool/wrong-find-id");
  } else {
    unit;
  };

  let acc = getAccountById(id_to_find, ps);
  let addr = getAddrById(acc.user_id, ps);
  let user_wtt = getCurrentRoundWttUser(addr, ps);
  if (user_wtt.wtt_amt == 0n) {
    // If user has 0 wtt - he not staked in this round. that means that we need next lucky challenger
    getWinner(id_to_find + 1n, prob, opts, ps);
  } else {
    unit;
  };
  // TODO: math
  let user_prob = user_wtt.wtt_amt / ps.round_acc_wtt;
  if (user_prob < prob) {
    if (id_to_find + 1n == opts.initial_id) {
      // if current user is last in search list and his probability not fit's to condition
      (failwith("Pool/cant-find-winner") : nat); // this situation shouldn't happen, 
    } else {
      getWinner(id_to_find + 1n, prob, opts, ps);
    };
  } else {
    acc.user_id;
  };
  // 1n;
};
*/
