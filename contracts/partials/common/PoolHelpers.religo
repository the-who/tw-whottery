let getDefaultAcc = ((ps) : (pool_storage_type)) : pool_account_type => {
  let default_user_wtt : cur_round_wtt_type = {
    total_wtt: 0n,
    wtt_amt  : 0n,
    round_id : ps.round_id
  };
  // Dummy object for new accounts
  let new_default_acc : pool_account_type = {
    user_id            : ps.next_user_id,
    balance            : 0n,
    last_date_provided : Tezos.now,
    last_date_withdrawn: Tezos.now,
    reward             : { reward_amt : 0n },
    wtt_balance        : default_user_wtt,
    clt_balance        : 0n,
    clt_claimed        : 0n,
    last_clt_request   : Tezos.now,
#if FA2_STANDARD_ENABLED
    allowances         : (Set.empty : set (address))
#else
    allowances         : (Map.empty : map (address, nat))
#endif
  };
  new_default_acc;
}

// Helper function to get account
let getAccount = ((addr, ps) : (address, pool_storage_type)) : pool_account_type => {
  unoptOr(Big_map.find_opt(addr, ps.ledger), getDefaultAcc(ps));
};

let getCreatorAccount = ((asset_amt, ps) : (asset_amt_type, pool_storage_type)) : pool_account_type => {
  let def_a = getDefaultAcc(ps);
  let creator_acc = {
    ...def_a,
    balance : asset_amt,
    last_date_provided : Tezos.now,
    clt_balance : ps.total_clt, // Pool creator own's all CLT tokens on start, but can't transfer/sell them =)
    last_clt_request : Tezos.now
  };
  creator_acc;
};

let getAddrById = ((id, ps) : (nat, pool_storage_type)) : address => {
  unopt(Big_map.find_opt(id, ps.id_to_addr), PoolCore.addr_not_found_by_id);
};

let getAccountById = ((id, ps) : (nat, pool_storage_type)) => {
  let addr = getAddrById(id, ps);
  getAccount(addr, ps);
};
// WTT amount for current round
let getUserWttForRound = ((acc, ps) : (pool_account_type, pool_storage_type)) : nat => {
  let latest_provide_time = getLatest(ps.round_start, acc.last_date_provided);
  let staked_in_round = abs (Tezos.now - latest_provide_time);
  let uwtt = acc.wtt_balance;
  let getUserWtt = () => if (acc.wtt_balance.round_id == ps.round_id) { uwtt.wtt_amt; } else { 0n; };
  let round_wtt = acc.balance * staked_in_round + getUserWtt();
  round_wtt;
};

let getUserTotalWtt = ((acc, _ps) : (pool_account_type, pool_storage_type)) : nat => {
  let old_user_total_wtt = acc.wtt_balance.total_wtt;
  let last_staked_time = abs (Tezos.now - acc.last_date_provided);
  let total_user_wtt = old_user_total_wtt + last_staked_time * acc.balance;
  total_user_wtt;
};

// mint
let addCltToUser = ((acc, ps) : (pool_account_type, pool_storage_type)) => {
  let clt_amt = getUserTotalWtt(acc, ps) / (ps.round_id * 10_000_000_000n);
  if (ps.total_clt + clt_amt < max_clt_supply) {
    {...acc, clt_balance : acc.clt_balance + clt_amt}
  } else {
    acc;
  };
};
// burn
let subCltFromUser = ((acc, _ps) : (pool_account_type, pool_storage_type)) => {
  let clt_amt = acc.clt_balance * 30 / 100;
  {...acc, clt_balance : abs(acc.clt_balance - clt_amt)}
}

// ur - user reward, pr - pool reward
let getUserReward = ((total_reward) : (nat)) : (nat, nat) => {
  let ur = abs (total_reward / 100 * 60);
  let pr = abs (total_reward - ur);
  (ur, pr);
};

let userIncomeShare = ((ps) : (pool_storage_type)) : nat => {
  let shares_purchased : nat = Tezos.amount / 1mutez * ps.total_asset_pool / ps.tezos_pool;
  shares_purchased;
};
let userOutcomeTezTok = ((asset_amt, ps) : (asset_amt_type, pool_storage_type)) : (nat, nat) => {
  // calculate amount of token's sent to user
  let tez_divested : nat = ps.tezos_pool * asset_amt / ps.total_asset_pool;
  let tokens_divested : nat = ps.token_pool * asset_amt / ps.total_asset_pool;
  (tez_divested, tokens_divested);
};

let extractInputAssets = ((p) : (enter_pool_params)) : input_assets_type => {
#if ACCEPT_TOKENS_ENABLED
  onlyToken();
  InToken(p);
#endif
#if ACCEPT_TEZOS_ENABLED
  onlyTezos(p);
  InTezos(p);
#endif
#if ACCEPT_TEZTOK_ENABLED
  InTezTok(p);
#endif
};

let extractOutputAssets = ((p) : (exit_pool_params)) : output_asset_type => {
#if ACCEPT_TOKENS_ENABLED
  OutToken(p);
#endif
#if ACCEPT_TEZOS_ENABLED
  OutTezos(p);
#endif
#if ACCEPT_TEZTOK_ENABLED
  OutTezTok(p[2]); //(p[0], p[1], p[2])
#endif
};


let addAssets = ((p, acc, ps) : (enter_pool_params, pool_account_type, pool_storage_type)) : (pool_account_type, asset_amt_type, (nat, nat)) => {
  let tezos_amt = Tezos.amount / 1mutez;
  let income = switch(extractInputAssets(p)) {
    | InTezos (_n)  => Tezos.amount / 1mutez
    | InToken (a)   => a
    | InTezTok (_n) => userIncomeShare(ps)
  };
  let out_v = switch(extractInputAssets(p)) {
    | InTezos (_n)  => (tezos_amt, 0n)
    | InToken (_a)  => (0n, income)
    | InTezTok (_n) => (tezos_amt, income)
  };

  // let cur_wtt = getCurrentRoundWttUserAmt(acc, ps);
  // let (current_user_wtt, last_wtt_amt) = accumulatedWtt(cur_wtt, acc.balance, ps.round_start, ps.last_date_provided);
  let wtt_amt = getUserWttForRound(acc, ps);
  let total_wtt = getUserTotalWtt(acc, ps);
  let new_bal = acc.balance + income;
  ({
    ...acc,
    balance            : new_bal,
    wtt_balance        : {
      total_wtt : total_wtt,
      wtt_amt   : wtt_amt,
      round_id  : ps.round_id
    },
    last_date_provided : Tezos.now
  }, income, out_v);
};

let subAssets = ((p, acc, ps) : (exit_pool_params, pool_account_type, pool_storage_type)) : (pool_account_type, asset_amt_type, (nat, nat)) => {
  // checkWithdrawableAmt(p, acc, ps);
  let outcome: nat = switch(extractOutputAssets(p)) {
    | OutTezos (a)  => {
      assert_with_error(acc.balance >= a, PoolCore.insufficient_funds);
      a;
    }
    | OutToken (a)  => {
      assert_with_error(acc.balance >= a, PoolCore.insufficient_funds);
      a;
    }
    | OutTezTok (a) => {
      assert_with_error(acc.balance >= a, PoolCore.insufficient_funds);
      a;
    }
  };
  let new_bal = abs(acc.balance - outcome);
  let out_v = switch (extractOutputAssets(p)) {
    | OutTezos (_a)  => (outcome, 0n)
    | OutToken (_a)  => (0n, outcome)
    | OutTezTok (_a) => userOutcomeTezTok(outcome, ps)
  };
  // let cur_wtt = getCurrentRoundWttUserAmt(acc, ps);
  // TODO: slashing of current WTT for withdrawals?
  // let (current_user_wtt, last_wtt_amt) = accumulatedWtt(acc, ps);
  let wtt_amt = getUserWttForRound(acc, ps);
  let total_wtt = getUserTotalWtt(acc, ps);
  ({
    ...acc,
    balance : new_bal,
    wtt_balance : {
      total_wtt : total_wtt,
      wtt_amt   : wtt_amt, 
      round_id  : ps.round_id
    },
    last_date_withdrawn : Tezos.now,
    last_date_provided : Tezos.now
  }, outcome, out_v);
};

// let updateUserReward = ((addr, reward_amt, ps) : (address, nat, pool_storage_type)) => {
let updateUserReward = ((ps) : (pool_storage_type)) => {
  ps; // TODO
};

let updateWinners = ((winner_acc, ps) : (pool_account_type, pool_storage_type)) : round_winners_type => {
  let addr = getAddrById(winner_acc.user_id, ps);
  let rw = Big_map.add(ps.round_id, addr, ps.round_winners);
  rw;
};