/*
  This code based on Quipu-core-v2
  https://github.com/madfish-solutions/quipuswap-core-v2/blob/master/contracts/partial/common_helpers.ligo
*/
#include "../types/CommonTypes.religo"
let getBiggest = ((a, b) : (timestamp, timestamp)) : timestamp => {
  if (a > b) {
    a;
  } else {
    b;
  };
};

let getLatest = ((t1, t2) : (timestamp, timestamp)) : timestamp => {
  getBiggest(t1, t2);
};

let onlyToken = () => {
  assert_with_error(Tezos.amount == 0mutez, PoolCore.only_token_deposit);
};

let onlyTezos = ((amt) : (nat)) => {
  assert_with_error(amt == 0n, PoolCore.only_tezos_deposit);
};

let onlyAdmin = ((admin) : (address)) : unit => {
  assert_with_error(Tezos.sender == admin, Common.err_not_admin);
};

let unoptOr: (option(_a), _a) => _a = ((param, default) : (option(_a), _a)) : _a => {
  switch (param) {
    | Some (instance) => instance
    | None            => default
  };
};

let unopt: (option(_a), string) => _a = ((param, error) : (option(_a), string)) : _a => {
  switch(param) {
    | Some (i) => i
    | None     => (failwith(error) : _a)
  };
};
