//ERRORS

// let fa2_token_undefined : string = "FA2_TOKEN_UNDEFINED";
// let fa2_insufficient_balance : string = "FA2_INSUFFICIENT_BALANCE";
// let fa2_tx_denied : string = "FA2_TX_DENIED";
// let fa2_not_owner : string = "FA2_NOT_OWNER";
// let fa2_not_operator : string = "FA2_NOT_OPERATOR";
// let fa2_operators_not_supported : string = "FA2_OPERATORS_UNSUPPORTED";
// let fa2_receiver_hook_failed : string = "FA2_RECEIVER_HOOK_FAILED";
// let fa2_sender_hook_failed : string = "FA2_SENDER_HOOK_FAILED";
// let fa2_receiver_hook_undefined : string = "FA2_RECEIVER_HOOK_UNDEFINED";
// let fa2_sender_hook_undefined : string = "FA2_SENDER_HOOK_UNDEFINED";
let wrong_map : string = "WRONG_MAP";
let no_ep_on_ctrt : string = "NO_EP_ON_CONTRACT";

module PoolCore = {
  let user_not_found                     : string = "100";
  let addr_not_found_by_id               : string = "101";
  let only_token_deposit                 : string = "102";
  let only_tezos_deposit                 : string = "103";
  let insufficient_funds                 : string = "104";
  let no_reserves                        : string = "105";
  let cant_get_round_ep                  : string = "106";
  let not_empty_reserves                 : string = "107";
  let too_early_draw                     : string = "108";
  let err_unknown_func                   : string = "109";
  let cant_unpack_fn                     : string = "110";
  let cant_find_winner                   : string = "111";
};

module FactoryCore = {
  let func_already_set                   : string = "000";
}

module RewarderCore = {
  let not_exact_tez_amt                  : string = "200";
  let total_lower_than_pool_rew          : string = "201";
  let insufficient_balance               : string = "202";
  let cant_get_snd_ctrt                  : string = "203";
};

module Common = {
  let err_not_admin                      : string = "400";
  let err_not_pending_admin              : string = "401";
  let err_not_manager                    : string = "402";
  let err_not_pool_core                  : string = "403";
  let err_fa12_transfer_entrypoint_404   : string = "404";
  let err_fa2_transfer_entrypoint_404    : string = "405";
  let err_not_a_nat                      : string = "406";
  let err_fa12_balance_of_entrypoint_404 : string = "407";
  let err_fa2_balance_of_entrypoint_404  : string = "408";
  let err_wrong_token_type               : string = "409";
};

module Ids = {
  let start_pool = 0n;
  let enter_pool = 1n;
  let exit_pool = 2n;
  let draw = 3n;
  let round = 4n
  let set_prn_src = 5n;
  let set_round_timeframe = 6n;
  let set_reward_ctrt = 7n;
}
