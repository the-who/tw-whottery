#include "../types/IAdmin.religo"

// Errors
let contract_paused : string = "PAUSED";
let no_pending_admin : string = "NO_PENDING_ADMIN";
let not_admin : string = "NOT_ADMIN";

// Check if admin called contract
let isAdmin = ((admin_storage) : (admin_storage_type)) : unit => {
  if (Tezos.sender != admin_storage.admin) {
    (failwith(not_admin) : unit);
  } else {
    unit;
  }
};

// Check if contract paused
let isPaused = ((admin_storage) : (admin_storage_type)) : unit => {
  if (admin_storage.paused) {
    (failwith(contract_paused) : unit);
  } else {
    unit;
  }
};
// TODO: add signatures check ?
let handleAdminAction = ((adm_action, admin_storage) : (admin_actions, admin_storage_type)) : admin_storage_type => {
  isAdmin(admin_storage);
  switch (adm_action) {
    | SetPaused (new_state) => {
      { ...admin_storage, paused : new_state };
    }
    | SetAdmin (new_admin) => {
      { ...admin_storage, pending_admin : Some (new_admin) };
    }
    | ConfirmAdmin => {
      let pa = switch (admin_storage.pending_admin) {
        | Some (x) => x
        | None => (failwith(no_pending_admin) : address)
      };
      let new_st = { ...admin_storage, admin : pa, pending_admin : (None : option (address)) };
      new_st;
    }
  }
};

let setPrnSrc = ((p, ps, _this) : (pool_admin_action, pool_storage_type, address)) : return_type => {
  let new_ps = switch(p) {
    | SetPrnSrc (new_prn_src) => {
      let new_ps = { ...ps, prn_src : new_prn_src };
      new_ps;
    }
    | SetRoundTimeframe (_n) => ps
    | SetRewardContract (_n) => ps
  };
  (noOps, new_ps);
};

let setRoundTimeframe = ((p, ps, _this) : (pool_admin_action, pool_storage_type, address)) : return_type => {
  let new_ps = switch(p) {
    | SetPrnSrc (_n)         => ps
    | SetRoundTimeframe (params) => {
      let new_ps = { ...ps, round_timeframe : params };
      new_ps;
    }
    | SetRewardContract (_n) => ps
  };
  (noOps, new_ps);
};

let setRewardContract = ((p, ps, _this) : (pool_admin_action, pool_storage_type, address)) : return_type => {
  let new_ps = switch(p) {
    | SetPrnSrc (_n)         => ps
    | SetRoundTimeframe (_n) => ps
    | SetRewardContract (new_reward_ctrt) => {
      let new_ps = { ...ps, reward_rec_contract : new_reward_ctrt };
      new_ps;
    };
  };
  (noOps, new_ps);
};
