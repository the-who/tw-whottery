let checkReqs = ((token, fs) : (token_identifier, factory_storage_type)) => {
  switch(Big_map.find_opt(token, fs.token_to_pool)) {
    | Some (_n) => (failwith("Factory/pool-launched") : unit)
    | None => unit
  };
};

let checkSelfCall = () => {
  assert_with_error (Tezos.sender == Tezos.self_address, "Pool/Not-self-call");
};

let checkProvidedTezos = () => {
  // XTZ provided
  assert_with_error (Tezos.amount >= 1mutez, "Pool/no-xtz");
};
let checkTokensAmt = ((token_amount) : (asset_amt_type)) => {
  // tokens provided
  assert_with_error (token_amount >= 1n, "Pool/no-tokens");
};
let checkSharesAmt = ((shares_amt) : (asset_amt_type)) => {
  // shares provided
  assert_with_error (shares_amt >= 1n, "Pool/no-shares");
};

// check preconditions
let checkStartPreconditions = ((token_amount, ps) : (asset_amt_type, pool_storage_type)) => {
  assert_with_error(ps.tezos_pool == 0n && ps.token_pool == 0n, PoolCore.not_empty_reserves);
#if ACCEPT_TOKENS_ENABLED
  checkTokensAmt(token_amount);
  onlyToken();
#endif
#if ACCEPT_TEZOS_ENABLED
  checkProvidedTezos();
  onlyTezos(token_amount);
#endif
#if ACCEPT_TEZTOK_ENABLED
  checkTokensAmt(token_amount);
  checkProvidedTezos();
#endif
};

let checkTezAmt = ((tez_amt) : (tez)) => {
  assert_with_error (tez_amt >= 1mutez, "Pool/small-amount");
};

let checkDrawTime = ((ps) : (pool_storage_type)) => {
  assert_with_error((Tezos.now > (ps.round_start + int(ps.round_timeframe))), PoolCore.too_early_draw);
};

let isRndSrc = ((ps) : (pool_storage_type)) => {
  assert_with_error(ps.prn_src == Tezos.sender, "Pool/wrong-prn-sender");
};
