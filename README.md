# Tezos DEFI Hackathon 2022 / Quipuswap challenge
## TheWho team contracts repo
---
## Description
### Main idea
In most cases algorythm is very simple:
- There should be some token, which can be used in some kind of "staking/farming" process.
- Accept some amount of this token from user.
- Call some entrypoint on farm to "stake" this tokens. In some cases this step can be ommited.
- Wait for `pool_storage.timeframe` seconds.
- Call "claim" on farm. Distribute rewards in any way.
---
### Project structure
Structure of contracts is similar to [quipuswap-core](https://github.com/madfish-solutions/quipuswap-core). Instead of DEX'es this project uses Pool's as abstraction.
The general part of the algorithm is placed in the `MethodPools.religo` file. It describes the functions of initializing the pool, depositing funds into the pool, withdrawing funds from the pool, the Draw function, which is responsible for receiving awards and distributing them between the winner and the pool. In the current version, the Draw entry point takes the winner's ID as input. All logic for choosing a winner must be done off-chain. Also, each pool contains its own token, for the operation of which points of input have been added that meet the FA1.2 and FA2 standards. This is NOT an analogue of Quipu Shares, the token will be mentioned at the very bottom. [^1]
 

Specific implementations are currently in the files `SaresTo[something].religo`
The easiest way to execute such a lottery, in my opinion, is to accept Quipu Shares, the possession of which already generates income for the pool. This implementation is described in the files:
- `FactoryFA1.2_S2Q.religo`, `FactoryFA2_S2Q.religo` - factory contracts that issue lottery pools.
- `PoolFA1.2_S2Q.religo`, `PoolFA2_S2Q.religo` - lottery pools.
- `SharesToQuipu.religo`, `ISharesToQuipu.religo` - implementation of receiving / withdrawing funds, receiving rewards.(specific for each kind of lottery pool)
Also, the pool can accept Quipu Shares, invest them in Crunchy Farms (or any other type of farms with a similar principle of operation), then act according to the general algorithm (receive rewards, distribute). An example implementation is in the files: TODO
For a number of reasons, the implementation for Crunchy Farms has not been tested on live contracts.


[^1]: Each pool keeps track of how many tokens and how long the user has held them in the pool. Variables containing `WTT` in the name just reflect this indicator. At the moment, the implementation of `WTT` is in the test stage and its use is left to the conscience of the one who calls the Draw entry point (this can be a contract administrator or another source of random numbers). In fact, you can calculate WTT off-chain according to a principle known to everyone in advance, but, in my opinion, on-chain execution also has the right to life.
It is also planned to introduce some CLT token, which will be paid in proportion to the number of tokens, rounds and rounds won. This token will somehow increase the chance of winning. More specific implementations of the token need additional testing and remain the responsibility of the lottery developer. Now the entry points that comply with the FA1.2 and FA2 standards have been added specifically for operating the CLT token.

Factories and Pools have several modes:
- Token standart switch `FA2_STANDARD_ENABLED` to switch between accepting FA1.2 and FA2 token's.
- Accept token's switch `ACCEPT_TOKENS_ENABLED`. Same purpose as for `ACCEPT_TEZOS_ENABLED` switch. In most cases should be turned ON. For example, if pool accepts Quipu shares(which are FA tokens by themself) we need to turn it ON. In case of Juster common pools there is no need in this flag. In case of route users with 1 asset in Loto pools both flags should be ON.
- Accept tezos switch `ACCEPT_TEZOS_ENABLED`. This switch controls should contract accept XTZ or not. I think this could be useful for pool's where only XTZ accepted(Juster.fi for example). If we don't need to accept token's - just skip `ACCEPT_TOKENS_ENABLED` switch.
- Claim switches. They are used as source of reward's claim and fund's investment if this is necessary.
- - `CLAIM_QUIPU_ENABLED` - accept Quipu shares and claim reward's from Quipuswam Tezos-Token DEX'es once per round.
- - `CLAIM_CRUNCHY_ENABLED` - NOT IMPLEMENTED YET! Accept Quipu shares, invest them in Crunchy Farms. This is not tested yet, but it should work with minimal editions

### Mechanics to implement
Lottery uses two token's. WTT and CLT.
WTT - token that represents weight of user stake in current round. 0 - if user not provided fund's in pool for current round. For now calculated as amount of coins provided multiplied by amount of seconds that this funds were placed in pool. WTT amount calculated on every withdraw, I think there should be some kind of additional fee for "fast" waithdrawal's. WTT is temporary, non-transferrable token. It used only to calculate user probability to be a winner and main purpose of this value - make draw more fair.
CLT - token that represents count of milestones reached by users. For example, amount of rounds without withdrawal's multiplied/divided or smth else by total staked WTT or any other kind of math magic. It would be used to slightly increase users chance to win. At pool's start all CLT would be credited on balance of creator, but he will be allowed to spend only some part. Other token's will be distributed automaticaly from his account.


