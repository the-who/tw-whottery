import { MichelsonMap } from '@taquito/michelson-encoder';
import { join } from 'path';
import { execSync } from 'child_process';
import * as fs from 'fs';
import { deployContract, appendToEnv, getLigoPath } from '../utils';
import { factoryStorage } from '../storage/Factory';
import {
  adminFunctions,
  poolFunctions,
  tokenFunctions,
} from '../storage/Functions';
import { confirmOperation, IOpts } from './confirmation';
import {
  Operation,
  OperationBatch,
  OpKind,
  ParamsWithKind,
  TezosToolkit,
} from '@taquito/taquito';
import { IParamsObj } from './types';

export const deployFunctions = async (
  Tezos: TezosToolkit,
  address: string,
  standard: string,
  factoryContractPath: string,
) => {
  const c = await Tezos.contract.at(address);
  const store = await c.storage();
  // await uploadAdminFunctions(
  //   Tezos,
  //   adminFunctions,
  //   store,
  //   factoryContractPath,
  //   address,
  // );
  // await uploadPoolFunctions(
  //   Tezos,
  //   poolFunctions,
  //   store,
  //   factoryContractPath,
  //   address,
  // );
  // await uploadTokenFunctions(
  //   Tezos,
  //   tokenFunctions[standard],
  //   store,
  //   factoryContractPath,
  //   address,
  // );
  await uploadLambdasBytes(
    Tezos,
    poolFunctions,
    store,
    factoryContractPath,
    address,
    'lambdas',
    'setPoolFunction',
  );
  await uploadLambdasBytes(
    Tezos,
    adminFunctions,
    store,
    factoryContractPath,
    address,
    'lambdas',
    'setAdminFunction',
  );
  await uploadLambdasBytes(
    Tezos,
    tokenFunctions[standard],
    store,
    factoryContractPath,
    address,
    'token_lambdas_b',
    'setTokenFunction',
  );
};

export const deployFactory = async (
  params,
  opts: IOpts = {},
  paramsObj: IParamsObj,
) => {
  const {
    standard,
    farmSuffix,
    compiledFolder,
    contractsFolder,
    factoryAddress,
  } = paramsObj;
  const contName = `Factory${standard}${farmSuffix}`;
  const compiledContractName = `${contName}.tz`;
  const contractName = `${contName}.religo`;
  const factoryContractPath = join(contractsFolder, contractName);
  const compiledFactoryContractPath = join(
    compiledFolder,
    compiledContractName,
  );
  const factoryAddrEnvName = `FACTORY_${standard}${farmSuffix}`;
  const { Tezos } = params;
  if (factoryAddress && !opts.force) {
    await deployFunctions(Tezos, factoryAddress, standard, factoryContractPath);
    console.warn(
      `${factoryAddrEnvName} already filled with: ${factoryAddress}. All pool functions uploaded.`,
    );
    return paramsObj;
  }
  try {
    factoryStorage.metadata = MichelsonMap.fromLiteral({
      '': Buffer.from(
        'tezos-storage://' + process.env.METADATA_CONTRACT_ADDRESS + '/who',
        'ascii',
      ).toString('hex'),
    });
    const factoryCode = fs.readFileSync(compiledFactoryContractPath, 'utf-8');
    const result = await deployContract(Tezos, factoryCode, factoryStorage);
    appendToEnv(`\n${factoryAddrEnvName}="${result.address}"`);
    await deployFunctions(Tezos, result.address, standard, factoryContractPath);
    console.log(`${standard} ${farmSuffix} factory`, result.address);
    return {
      ...paramsObj,
      factoryAddress: result.address,
    };
  } catch (e) {
    console.error(e);
  }
};

export const compileLigoFuncAsParam = (params, funcObj) => {
  const { factoryPath, epName } = params;
  const { index, name } = funcObj;
  const command = `/bin/bash ${getLigoPath()} compile parameter --michelson-format "json" ${factoryPath} -e main --no-warn '${epName}({ func : ${name}, index : ${index}n })'`;
  const out = execSync(command);
  // console.log(out);
  return out;
};

export const compileLigoFuncAsBytes = (params, funcObj) => {
  const { contractPath } = params;
  const { name } = funcObj;
  const command = `${getLigoPath()} compile expression reasonligo 'Bytes.pack(${name})' --michelson-format json --init-file ${contractPath} --protocol hangzhou`;
  const out = JSON.parse(execSync(command).toString());
  // console.log(out);
  return out;
};
const isFnUploaded = async (store, index, lambdasBigmapName) => {
  try {
    // console.log(`$func already at ${index} idx`);
    const isExists = await store[lambdasBigmapName].get(index);
    return !!isExists;
  } catch {
    return false;
  }
};
export const uploadLambdasBytes = async (
  Tezos: TezosToolkit,
  fns: { index: number; name: string }[],
  store: any,
  contractPath: string,
  contractAddress: string,
  lambdasBigmapName: string,
  methodName: string, // SetAdminFunc|SetPoolFunc or other SetXXXFunc methods
) => {
  // const nameToActionName = (n: string): string =>
  //   `${n[0].toUpperCase()}${n.slice(1)}`;
  const contract = await Tezos.contract.at(contractAddress);
  let params: ParamsWithKind[] = [];
  for (const fn of fns) {
    const { index, name } = fn;
    if (await isFnUploaded(store, index, lambdasBigmapName)) continue;
    console.log(`Uploading fn ${name} at id: ${index}`);

    const funcBytes = compileLigoFuncAsBytes({ contractPath }, fn);
    const p: ParamsWithKind = {
      kind: OpKind.TRANSACTION,
      ...contract.methods[methodName](
        funcBytes.bytes,
        index,
      ).toTransferParams(),
    };
    params.push(p);
  }
  const batch: OperationBatch = Tezos.contract.batch(params);
  const operation: Operation = await batch.send();

  await confirmOperation(Tezos, operation.hash);
};

export const uploadAdminBytesFns = async (
  Tezos: TezosToolkit,
  adminFunctions: any[],
  store: any,
  factoryContractPath: string,
  factoryAddress: string,
) => {
  return await uploadLambdasBytes(
    Tezos,
    adminFunctions,
    store,
    factoryContractPath,
    factoryAddress,
    'admin_lambdas',
    'setAdminFunction',
  );
};

export const uploadAdminFunctions = async (
  Tezos: TezosToolkit,
  adminFunctions: any[],
  store: any,
  factoryContractPath: string,
  factoryAddress: string,
) => {
  if (process.env.SKIP_ADM_FUNC) return;
  const last = adminFunctions[adminFunctions.length - 1];
  isFnUploaded(store, last, 'admin_lambdas');
  for (const adminFunc of adminFunctions) {
    const { index, name } = adminFunc;
    console.log(index, name);
    const pf = await store['admin_lambdas'].get(index);
    if (pf) {
      console.log(`${name} func already at ${index} idx`);
      continue;
    }
    const funcCode = compileLigoFuncAsParam(
      { factoryPath: factoryContractPath, epName: 'SetAdminFunction' },
      adminFunc,
    );
    const operation = await Tezos.contract.transfer({
      to: factoryAddress,
      amount: 0,
      parameter: {
        entrypoint: 'setAdminFunction',
        value: JSON.parse(funcCode.toString()).args[0].args[0],
      },
    });
    await confirmOperation(Tezos, operation.hash);
  }
};

export const uploadPoolFunctions = async (
  Tezos: TezosToolkit,
  poolFunctions: any[],
  store: any,
  factoryContractPath: string,
  factoryAddress: string,
) => {
  const last = poolFunctions[poolFunctions.length - 1];
  const isExists = await store['pool_lambdas'].get(last.index);
  if (isExists) {
    console.log('All pool functions already in store');
    return;
  }
  for (const poolFunc of poolFunctions) {
    const { index, name } = poolFunc;
    console.log(index, name);
    const pf = await store['pool_lambdas'].get(index);
    if (pf) {
      console.log(`${name} func already at ${index} idx`);
      continue;
    }
    const funcCode = compileLigoFuncAsParam(
      { factoryPath: factoryContractPath, epName: 'SetPoolFunction' },
      poolFunc,
    );
    const operation = await Tezos.contract.transfer({
      to: factoryAddress,
      amount: 0,
      parameter: {
        entrypoint: 'setPoolFunction',
        value: JSON.parse(funcCode.toString()).args[0].args[0],
      },
    });
    await confirmOperation(Tezos, operation.hash);
  }
};
export const uploadTokenFunctions = async (
  Tezos: TezosToolkit,
  tokenFunctions: any[],
  store: any,
  factoryContractPath: string,
  factoryAddress: string,
) => {
  if (process.env.SKIP_TOKEN_FUNC) return;
  const last = tokenFunctions[tokenFunctions.length - 1];
  const isExists = await store['token_lambdas'].get(last.index);
  if (isExists) {
    console.log('All token functions already in store');
    return;
  }
  console.log('Last: ', last);
  for (const tokenFunc of tokenFunctions) {
    const { index, name } = tokenFunc;
    console.log(index, name);
    const tf = await store['token_lambdas'].get(index);
    if (tf) {
      console.log(`${name} func already at ${index} idx`);
      continue;
    }
    const funcCode = compileLigoFuncAsParam(
      { factoryPath: factoryContractPath, epName: 'SetTokenFunction' },
      tokenFunc,
    );
    const operation = await Tezos.contract.transfer({
      to: factoryAddress,
      amount: 0,
      parameter: {
        entrypoint: 'setTokenFunction',
        value: JSON.parse(funcCode.toString()).args[0].args[0],
      },
    });
    await confirmOperation(Tezos, operation.hash);
  }
};
