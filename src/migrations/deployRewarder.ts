import { importKey } from '@taquito/signer';
import * as fs from 'fs';
import { deployContract, appendToEnv } from '../utils';
import { rewarderStorage } from '../storage/RewarderStorage';
import { alice } from '../accounts';
import { IOpts } from './confirmation';
import { MichelsonMap } from '@taquito/taquito';
// import { IParamsObj } from './types';

const tokenEnvName = `TOKEN_`;
const envName = 'REWARDER_ADDRESS';

// const fa12 = process.env[`${tokenEnvName}_FA12`] || 'KT1Sw95x26Fnh4hApBvMCtqv2DNxNzTUKB38';
// const fa2 = process.env[`${tokenEnvName}_FA2`] || 'KT19AQbd4rZ3e6B224YybenPQzAFDzGM2jRn';

export const deployRewarder = async (
  params,
  opts: IOpts = {},
) => {
  const { Tezos } = params;
  const address = process.env[envName];
  if (!address || opts.force) {
    const codePath = `${process.env.COMPILED_FOLDER}/RewardsVault.tz`;
    if (!codePath) process.exit(2);
    await importKey(Tezos, alice.sk);
    try {
      const code = fs.readFileSync(codePath, 'utf-8');
      rewarderStorage.tokens = MichelsonMap.fromLiteral({
        '1': { tez: undefined },
        // '2': { fa12 },
        // '3': { fa2: { address: fa2, id: 0 } },
      });
      const result = await deployContract(Tezos, code, rewarderStorage);
      console.log(`Rewarder: `, result);
      appendToEnv(`\n${envName}="${result.address}"`);
      return result.address;
    } catch (e) {
      console.error(e);
    }
  } else {
    console.error(`${envName} already filled with: ${address}`);
  }
};
