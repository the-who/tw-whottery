import {
  addLiquidity,
  batchify,
  findDex,
  toContract,
  Token,
} from '@quipuswap/sdk';
// import { investLiquidity } from '@quipuswap/sdk/dist/contracts/dex';
import { IOpts } from './confirmation';
import { IParamsObj } from './types';

const confirmOps = process.env.CONFIRM_OPS === 'true';

import { alice, bob, eve, my, fred } from '../accounts';
import { importKey } from '@taquito/signer';

const skip = process.env.SKIP_LIQ === 'true';

export const provideLiquidity = async (
  params,
  opts: IOpts,
  paramsObj: IParamsObj,
): Promise<IParamsObj> => {
  if (skip) return;
  const { Tezos } = params;
  await provide(params, opts, paramsObj);
  for (const user of [my, bob, eve, fred]) {
    await importKey(Tezos, user.sk);
    await provide(params, opts, paramsObj);
  }
  await importKey(Tezos, alice.sk);
  return paramsObj;
};
export const provide = async (
  params,
  opts: IOpts,
  paramsObj: IParamsObj,
): Promise<IParamsObj> => {
  const { Tezos } = params;
  const { standard, qFactories, tokenAddress } = paramsObj;
  const token: Token = { contract: tokenAddress };
  if (standard === 'FA2') token.id = 0;
  try {
    const dex = await findDex(Tezos, qFactories, token);
    // const tezAmt = 10_000_000;
    const tokenAmt = 500;

    const addLiquidityParams = await addLiquidity(Tezos, dex, {
      tokenValue: tokenAmt,
    });
    const op = await batchify(
      Tezos.wallet.batch([]),
      addLiquidityParams,
    ).send();

    console.info(op.hash);
    if (confirmOps) await op.confirmation();
    console.info('Liquidity provided');
    return paramsObj;
  } catch (e) {
    console.error(e);
  }
};
