import {
  Token,
  initializeLiquidity,
  batchify,
  findDexNonStrict,
} from '@quipuswap/sdk';
import { IOpts } from './confirmation';
import { appendToEnv } from '../utils';
import { IParamsObj } from './types';

/**
 * Launch Quipu DEX for test token
 */
export const launchDexForToken = async (
  params,
  opts: IOpts = {},
  paramsObj: IParamsObj,
): Promise<IParamsObj> => {
  const { tokenAddress, dexAddress, standard, qFactories, envNames } = paramsObj;
  const dexAddrEnvName = envNames.dexEnvName;

  let token: Token = {
    contract: tokenAddress,
    // id: 0,
  };

  if (standard === 'FA2') {
    token.id = 0;
  }
  const { Tezos } = params;
  if (dexAddress && !opts.force) {
    console.warn(`${dexAddrEnvName} already filled with: ${dexAddress}`);
    return;
  }
  const dex = await findDexNonStrict(Tezos, qFactories, token);
  if (dex) {
    appendToEnv(`\n${dexAddrEnvName}="${dex.contract.address}"`);
    console.warn(`Found DEX in Quipu factory. Appended to .env file.`);
    return paramsObj;
  }
  try {
    const tokenValue = 400;
    const tezValue = 10_000_000;

    const initializeLiquidityParams = await initializeLiquidity(
      Tezos,
      qFactories,
      token,
      tokenValue,
      tezValue,
    );

    const op = await batchify(
      Tezos.contract.batch([]),
      initializeLiquidityParams,
    ).send();

    console.info(op.hash);
    await op.confirmation();
    const dex = await findDexNonStrict(Tezos, qFactories, token);
    appendToEnv(`\n${dexAddrEnvName}="${dex.contract.address}"`);
    console.info('Complete');
    return {
      ...paramsObj,
      dexAddress: dex.contract.address,
    };
  } catch (err) {
    console.error(err);
  }
  return paramsObj;
};
