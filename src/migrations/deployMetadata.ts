import { importKey } from '@taquito/signer';
import * as fs from 'fs';
import { deployContract, appendToEnv } from '../utils';
import { metadataStorage } from '../storage/MetadataStorage';
import { alice } from '../accounts';
import { IOpts } from './confirmation';

export const deployMetadata = async (params, opts: IOpts = {}) => {
  const { Tezos } = params;
  const metaContrAddress = process.env.METADATA_CONTRACT_ADDRESS;
  if (!metaContrAddress || opts.force) {
    const codePath = process.env.METADATA_CONTRACT_PATH;
    if (!codePath) process.exit(2);
    await importKey(Tezos, alice.sk);
    try {
      const code = fs.readFileSync(codePath, 'utf-8');
      const result = await deployContract(Tezos, code, metadataStorage);
      console.log(`MetadataStorage: `, result);
      appendToEnv(`\nMETADATA_CONTRACT_ADDRESS="${result.address}"`);
      return result.address;
    } catch (e) {
      console.error(e);
    }
  } else {
    console.error(
      `METADATA_CONTRACT_ADDRESS already filled with: ${metaContrAddress}`,
    );
  }
};
