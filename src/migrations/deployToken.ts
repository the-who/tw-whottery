import * as fs from 'fs';
import { deployContract, appendToEnv } from '../utils';
import { join } from 'path';
import { IOpts } from './confirmation';
import { TokenFA12Storage } from '../storage/TokenFA12';
import { TokenFA2Storage } from '../storage/TokenFA2';
import { IParamsObj } from './types';

export const deployToken = async (
  params,
  opts: IOpts = {},
  paramsObj: IParamsObj,
): Promise<IParamsObj> => {
  const { Tezos } = params;
  const { standard, compiledFolder, tokenAddress, envNames } = paramsObj;
  const getStorage = () =>
    standard === 'FA2' ? TokenFA2Storage : TokenFA12Storage;
  const storage = getStorage();
  const contractName = `Token${standard}.tz`;
  console.log(compiledFolder, contractName);
  const codePath = join(compiledFolder, contractName);
  if (tokenAddress && !opts.force) {
    console.warn(
      `${envNames.tokenEnvName} already filled with: ${tokenAddress}`,
    );
    return paramsObj;
  }
  try {
    const code = fs.readFileSync(codePath, 'utf-8');
    const result = await deployContract(Tezos, code, storage);
    appendToEnv(`\n${envNames.tokenEnvName}="${result.address}"`);
    console.log(`${standard} token: `, result.address);
    return {
      ...paramsObj,
      tokenAddress: result.address,
    };
  } catch (e) {
    console.error(e);
  }
  return paramsObj;
};
