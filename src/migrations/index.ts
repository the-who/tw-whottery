const dotenv = require('dotenv');
dotenv.config();
import { importKey } from '@taquito/signer';
import { TezosToolkit } from '@taquito/taquito';
import { deployMetadata } from './deployMetadata';
import { deployToken } from './deployToken';
import { launchDexForToken } from './launchDexForToken';
import { deployFactory } from './deployFactory';
import { launchPoolForQuipuDex } from './launchPool';
import { IParamsObj } from './types';
import { alice } from '../accounts';
import { provideLiquidity } from './provideLiquidity';
import { deployRewarder } from './deployRewarder';
import { deployQFactory, deployQFunctions, deployQMetadata } from './deployQuipu';


// if (!process.env.TOKEN_STANDARD) {
//   console.error(`No TOKEN_STANDARD env`);
//   process.exit(1);
// }
// const standard = process.env.TOKEN_STANDARD as 'FA12' | 'FA2';
const farmSuffix = process.env.FARM_SUFFIX as '_S2Q';
const compiledFolder = process.env.COMPILED_FOLDER;
const contractsFolder = process.env.CONTRACTS_FOLDER;
const qContractsFolder = process.env.CONTRACTS_DEX_FOLDER;
const dontForcePools = process.env.DONT_FORCE_POOLS === 'true';

const Tezos = new TezosToolkit('https://hangzhounet.api.tez.ie');

const startStage = parseInt(process.env.START_STAGE);
const stageId = parseInt(process.env.STAGE);

const run = async (stageAction, tt, opts = {}, paramsObj: IParamsObj) => {
  return await stageAction({ Tezos: tt }, opts, paramsObj);
};

const runStages = async (standard: 'FA12' | 'FA2', stages: any[]) => {
  await importKey(Tezos, alice.sk);

  const qFactoryEnvName = `QFACTORY_${standard}`;
  const tokenEnvName = `TOKEN_${standard}`;
  const dexEnvName = `DEX_${standard}`;
  const factoryEnvName = `FACTORY_${standard}${farmSuffix}`;
  const poolEnvName = `POOL_${standard}${farmSuffix}`;
  const factories = {
    fa1_2Factory: process.env[factoryEnvName] || '',
    fa2Factory: process.env[factoryEnvName] || '',
  };
  const qFactories = {
    fa1_2Factory: process.env[qFactoryEnvName],
    fa2Factory: process.env[qFactoryEnvName],
  };
  
  let resObj: IParamsObj = {
    compiledFolder,
    contractsFolder,
    qContractsFolder,
    standard,
    farmSuffix,
    qFactories,
    factories,
    tokenAddress: process.env[tokenEnvName],
    dexAddress: process.env[dexEnvName],
    factoryAddress: process.env[factoryEnvName],
    qFactoryAddress: process.env[qFactoryEnvName],
    poolAddress: process.env[poolEnvName],
    envNames: {
      tokenEnvName,
      dexEnvName,
      factoryEnvName,
      poolEnvName,
    },
  };
  for (const id in stages) {
    // const fa12FactoryEnvName = `FACTORY_FA12${farmSuffix}`;
    // const fa2FactoryEnvName = `FACTORY_FA2${farmSuffix}`;
    // const fa12Factory = process.env[fa12FactoryEnvName];
    // const fa2Factory = process.env[fa2FactoryEnvName];

    // const factories = {
    //   fa1_2Factory: fa12Factory,
    //   fa2Factory: fa2Factory,
    // };
    if (stageId && parseInt(id) !== stageId) continue;
    if (parseInt(id) < startStage) continue;
    const stage = stages[id];
    console.log(stage);
    const res = await run(stage.action, Tezos, stage.opts, resObj);
    resObj = { ...resObj, ...res };
    if (standard === 'FA2' && resObj.factoryAddress) {
      resObj = {
        ...resObj,
        factories: { fa1_2Factory: '', fa2Factory: resObj.factoryAddress },
      };
    } else {
      resObj = {
        ...resObj,
        factories: { fa1_2Factory: resObj.factoryAddress, fa2Factory: '' },
      };
    }
  }
  console.log('All migrations done!');
  console.dir(resObj, { depth: 5 });
  return resObj;
};

(async () => {
  const standards: ('FA12' | 'FA2')[] = ['FA12', 'FA2'];

  if (!process.env.REWARDER_ADDRESS) {
    await runStages('FA12', [{ action: deployRewarder }]);
    console.log(`Rewarder deployed`);
    process.exit(0);
  }
  if (process.env.DEPLOY_QUIPU === 'true') {
    const quipuStages = [{ action: deployQMetadata }, {action: deployQFactory}];
    for (const standard of standards) {
      await runStages(standard, quipuStages);
    }
    process.exit(0);
  }

  const stages = [
    {
      action: deployMetadata,
    },
    {
      action: deployToken,
    },
    {
      action: launchDexForToken,
    },
    {
      action: provideLiquidity,
    },
    {
      action: deployFactory,
    },
    {
      action: launchPoolForQuipuDex,
    },
    {
      action: launchPoolForQuipuDex,
      opts: {
        force: !dontForcePools,
      },
    },
  ];
  for (const standard of standards) {
    await runStages(standard, stages);
  }
  console.log('All done!');
})();
