import { batchify, findDexNonStrict, toContract, Token } from '@quipuswap/sdk';
import { findPool, findPoolNonStrict, initPool } from '../common/libs/loto/lib';
import { IOpts } from './confirmation';
import { alice } from '../accounts';
import { appendToEnv } from '../utils';
import { IParamsObj } from './types';

const rewarder = process.env.REWARDER_ADDRESS || alice.pkh;
// if (!rewarder) {
//   console.error(`No rewarder address. Please deploy.`);
//   process.exit(1);
// }

export const launchPoolForQuipuDex = async (
  params,
  opts: IOpts = {},
  paramsObj: IParamsObj,
) => {
  const {
    standard,
    envNames,
    tokenAddress,
    // rewarderAddress,
    dexAddress,
    poolAddress,
    factories,
  } = paramsObj;

  const poolEnvName = envNames.poolEnvName;

  let token: Token = {
    contract: dexAddress,
  };

  if (standard === 'FA2') {
    token.id = 0;
  }
  const { Tezos } = params;
  if (poolAddress && !opts.force) {
    console.warn(`${poolEnvName} already filled with: ${poolAddress}`);
    return;
  }
  const currentPool = await findPoolNonStrict(Tezos, factories, token);
  if (currentPool && !opts.force) {
    appendToEnv(`\n${poolEnvName}="${currentPool.contract.address}"`);
    console.warn(`Found POOL in factory. Appended to .env file.`);
    return paramsObj;
  }
  try {
    const tokenValue = opts.force ? 10000 : 0;
    const tezValue = 0;

    const startPoolParams = await initPool(
      Tezos,
      factories,
      token,
      tokenValue,
      dexAddress,
      rewarder,
      alice.pkh,
      30 * 60,
    );

    const op = await batchify(Tezos.contract.batch([]), startPoolParams).send();

    console.info(op.hash);
    await op.confirmation();
    const pool = await findPool(Tezos, factories, token);
    if (!opts.force) appendToEnv(`\n${poolEnvName}="${pool.contract.address}"`);
    console.info('Complete');
    return {
      ...paramsObj,
      poolAddress: pool.contract.address,
    };
  } catch (err) {
    console.error(err);
  }
};
