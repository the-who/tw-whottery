import { importKey } from '@taquito/signer';
import { execSync } from 'child_process';
import * as fs from 'fs';
import { join } from 'path';
import { deployContract, appendToEnv, getLigoPath } from '../utils';
import { qMetadataStorage } from '../storage/QMetadataStorage';
import { alice } from '../accounts';
import { confirmOperation, IOpts } from './confirmation';
import { MichelsonMap, TezosToolkit } from '@taquito/taquito';
import {
  // adminFunctions,
  dexFunctions,
  tokenFunctions,
} from '../storage/QFunctions';
import { IParamsObj } from './types';
import { qFactoryStorage } from '../storage/QFactory';


export const compileQLigoFuncAsParam = (params, funcObj) => {
  const { qFactoryPath, epName } = params;
  const { index, name } = funcObj;
  const command = `/bin/bash ${getLigoPath()} compile parameter --michelson-format "json" ${qFactoryPath} -e main --no-warn '${epName}(record [ func = ${name}; index = ${index}n; ])'`;//'SetTokenFunction(record index =${tokenFunction.index}n; func = ${tokenFunction.name}; end)'
  const out = execSync(command);
  // console.log(out);
  return out;
};

export const deployQMetadata = async (params, opts: IOpts = {}) => {
  const { Tezos } = params;
  const metaContrAddress = process.env.QMETADATA_CONTRACT_ADDRESS;
  if (!metaContrAddress || opts.force) {
    const codePath = process.env.QMETADATA_CONTRACT_PATH;
    if (!codePath) process.exit(2);
    await importKey(Tezos, alice.sk);
    try {
      const code = fs.readFileSync(codePath, 'utf-8');
      const result = await deployContract(Tezos, code, qMetadataStorage);
      console.log(`QMetadataStorage: `, result);
      appendToEnv(`\nQMETADATA_CONTRACT_ADDRESS="${result.address}"`);
      return result.address;
    } catch (e) {
      console.error(e);
    }
  } else {
    console.error(
      `QMETADATA_CONTRACT_ADDRESS already filled with: ${metaContrAddress}`,
    );
  }
};

export const deployQFactory = async (
  params,
  opts: IOpts = {},
  paramsObj: IParamsObj,
) => {
  const {
    standard,
    farmSuffix,
    qContractsFolder,
    // factoryAddress,
    qFactoryAddress,
  } = paramsObj;
  const contName = `Factory${standard}`;
  const compiledFolder = process.env.CONTRACTS_DEX_FOLDER;
  const compiledContractName = `${contName}.tz`;
  const contractName = `${contName}.ligo`;
  const qFactoryContractPath = join(qContractsFolder, contractName);
  const compiledFactoryContractPath = join(
    compiledFolder,
    compiledContractName,
  );
  const qFactoryAddrEnvName = `QFACTORY_${standard}`;
  const { Tezos } = params;
  if (qFactoryAddress && !opts.force) {
    await deployQFunctions(
      Tezos,
      qFactoryAddress,
      standard,
      qFactoryContractPath,
    );
    console.warn(
      `${qFactoryAddrEnvName} already filled with: ${qFactoryAddress}. All pool functions uploaded.`,
    );
    return paramsObj;
  }
  try {
    qFactoryStorage.metadata = MichelsonMap.fromLiteral({
      '': Buffer.from(
        'tezos-storage://' + process.env.QMETADATA_CONTRACT_ADDRESS + '/who',
        'ascii',
      ).toString('hex'),
    });
    const factoryCode = fs.readFileSync(compiledFactoryContractPath, 'utf-8');
    const result = await deployContract(Tezos, factoryCode, qFactoryStorage);
    appendToEnv(`\n${qFactoryAddrEnvName}="${result.address}"`);
    await deployQFunctions(
      Tezos,
      result.address,
      standard,
      qFactoryContractPath,
    );
    console.log(`${standard} ${farmSuffix} factory`, result.address);
    return {
      ...paramsObj,
      qFactoryAddress: result.address,
    };
  } catch (e) {
    console.error(e);
  }
};

export const uploadDexFunctions = async (
  Tezos: TezosToolkit,
  poolFunctions: any[],
  store: any,
  qFactoryContractPath: string,
  qFactoryAddress: string,
) => {
  const last = poolFunctions[poolFunctions.length - 1];
  const isExists = await store['dex_lambdas'].get(last.index);
  if (isExists) {
    console.log('All pool functions already in store');
    return;
  }
  for (const poolFunc of poolFunctions) {
    const { index, name } = poolFunc;
    console.log(index, name);
    const pf = await store['dex_lambdas'].get(index);
    if (pf) {
      console.log(`${name} func already at ${index} idx`);
      continue;
    }
    const funcCode = compileQLigoFuncAsParam(
      { qFactoryPath: qFactoryContractPath, epName: 'SetDexFunction' },
      poolFunc,
    );
    const operation = await Tezos.contract.transfer({
      to: qFactoryAddress,
      amount: 0,
      parameter: {
        entrypoint: 'setDexFunction',
        value: JSON.parse(funcCode.toString()).args[0].args[0],
      },
    });
    await confirmOperation(Tezos, operation.hash);
  }
};
export const uploadQTokenFunctions = async (
  Tezos: TezosToolkit,
  tokenFunctions: any[],
  store: any,
  qFactoryContractPath: string,
  qFactoryAddress: string,
) => {
  if (process.env.SKIP_TOKEN_FUNC) return;
  const last = tokenFunctions[tokenFunctions.length - 1];
  const isExists = await store['token_lambdas'].get(last.index);
  if (isExists) {
    console.log('All token functions already in store');
    return;
  }
  console.log('Last: ', last);
  for (const tokenFunc of tokenFunctions) {
    const { index, name } = tokenFunc;
    console.log(index, name);
    const tf = await store['token_lambdas'].get(index);
    if (tf) {
      console.log(`${name} func already at ${index} idx`);
      continue;
    }
    const funcCode = compileQLigoFuncAsParam(
      { qFactoryPath: qFactoryContractPath, epName: 'SetTokenFunction' },
      tokenFunc,
    );
    const operation = await Tezos.contract.transfer({
      to: qFactoryAddress,
      amount: 0,
      parameter: {
        entrypoint: 'setTokenFunction',
        value: JSON.parse(funcCode.toString()).args[0],//.args[0],
      },
    });
    await confirmOperation(Tezos, operation.hash);
  }
};


export const deployQFunctions = async (
  Tezos: TezosToolkit,
  address: string,
  standard: string,
  qFactoryContractPath: string,
) => {
  const c = await Tezos.contract.at(address);
  const store = await c.storage();
  // await uploadAdminFunctions(
  //   Tezos,
  //   adminFunctions,
  //   store,
  //   factoryContractPath,
  //   address,
  // );
  await uploadDexFunctions(
    Tezos,
    dexFunctions,
    store,
    qFactoryContractPath,
    address,
  );
  await uploadQTokenFunctions(
    Tezos,
    tokenFunctions[standard],
    store,
    qFactoryContractPath,
    address,
  );
};

