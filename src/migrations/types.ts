import { StrictFactories } from '@quipuswap/sdk';

export interface IParamsObj {
  compiledFolder: string;
  contractsFolder: string;
  qContractsFolder: string;
  standard: 'FA12' | 'FA2';
  farmSuffix: '_S2Q' | '_S2Cr' | '_S2Loto';
  qFactories: StrictFactories;
  factories: StrictFactories;
  tokenAddress?: string;
  dexAddress?: string;
  factoryAddress?: string;
  qFactoryAddress?: string;
  poolAddress?: string;
  envNames: {
    tokenEnvName: string;
    dexEnvName: string;
    factoryEnvName: string;
    poolEnvName: string;
  };
}
