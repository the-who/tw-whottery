import { packData } from '@taquito/michel-codec';
import { MichelsonMap } from '@taquito/michelson-encoder';
import { char2Bytes } from '@taquito/tzip16';
import { my, alice, bob, eve, fred } from '../accounts';

const users = [my, alice, bob, eve, fred];

const amt = 2_500_000_000_000;
const totalSupply = amt * users.length;

export const TokenFA12Storage = {
  owner: alice.pkh,
  total_supply: totalSupply,
  ledger: MichelsonMap.fromLiteral({
    [my.pkh]: {
      balance: amt,
      allowances: new MichelsonMap(),
    },
    [alice.pkh]: {
      balance: amt,
      allowances: new MichelsonMap(),
    },
    [bob.pkh]: {
      balance: amt,
      allowances: new MichelsonMap(),
    },
    [eve.pkh]: {
      balance: amt,
      allowances: new MichelsonMap(),
    },
    [fred.pkh]: {
      balance: amt,
      allowances: new MichelsonMap(),
    },
  }),
  metadata: MichelsonMap.fromLiteral({
    // '': '68747470733a2f2f676973742e67697468756275736572636f6e74656e742e636f6d2f524f4b31302f30386331393364613435363263366661646566653866343533326632363930332f7261772f333733656535363836336336313834383336376463623135303334396466626537353562666631632f666131322d6d657461646174612e6a736f6e',
    '': Buffer.from('tezos-storage:here', 'ascii').toString('hex'),
    here: Buffer.from(
      JSON.stringify({
        name: 'Test FA12',
        symbol: 'WTT',
        decimals: 8,
        thumbnailUri: 'https://thewhobucket.s3.eu-central-1.amazonaws.com/favicon1.ico',
        version: 'v0.0.1',
        description: 'FA12 Test Token',
        authors: ['a', 'b', 'c'],
        homepage: 'https://thewho.com/',
        source: {
          tools: ['Ligo'],
          location: 'https://ligolang.org/',
        },
        interfaces: ['TZIP-7', 'TZIP-16'],
        errors: [],
      }),
      'ascii',
    ).toString('hex'),
  }),
  // token_metadata: MichelsonMap.fromLiteral({
  //   name: char2Bytes('TW2 test'),
  //   // authors: ['a', 'b', 'c'],
  //   homepage: char2Bytes('https://thewho.com/'),
  //   symbol: char2Bytes('TW1'),
  //   decimals: char2Bytes('6'),
  //   // shouldPreferSymbol: true,
  //   thumbnailUri:
  //     char2Bytes('https://thewhobucket.s3.eu-central-1.amazonaws.com/favicon1.ico'),
  //   // '': char2Bytes(
  //   //   JSON.stringify({
  //   //   }),
  //   // ),
  // }),
};
