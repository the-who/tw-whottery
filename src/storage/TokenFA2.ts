import { MichelsonMap } from '@taquito/michelson-encoder';
import { alice, bob, eve, my, fred } from '../accounts';

const users = [alice, bob, eve, my, fred];
const amt = 2_500_000_000_000;
const totalSupply = amt * users.length;

export const TokenFA2Storage = {
  ledger: MichelsonMap.fromLiteral({
    [my.pkh]: { balance: amt, allowances: [] },
    [alice.pkh]: { balance: amt, allowances: [] },
    [bob.pkh]: { balance: amt, allowances: [] },
    [eve.pkh]: { balance: amt, allowances: [] },
    [fred.pkh]: { balance: amt, allowances: [] },
  }),
  operators: new MichelsonMap(),
  token_metadata: MichelsonMap.fromLiteral({
    0: {
      token_id: 0,
      token_info: MichelsonMap.fromLiteral({
        0: Buffer.from(
          JSON.stringify({
            name: 'TW2 test',
            authors: ['a', 'b', 'c'],
            homepage: 'https://thewho.com/',
            symbol: 'TW2',
            decimals: 6,
            shouldPreferSymbol: true,
            thumbnailUri:
              'https://thewhobucket.s3.eu-central-1.amazonaws.com/favicon2.ico',
          }),
        ).toString('hex'),
      }),
    },
  }),
  metadata: MichelsonMap.fromLiteral({
    '': Buffer.from('tezos-storage:here', 'ascii').toString('hex'),
    here: Buffer.from(
      JSON.stringify({
        version: 'v0.0.1',
        description: 'Whottery FA2 Pool Token',
        name: 'Whottery Token FA2',
        authors: ['a', 'b', 'c'],
        homepage: 'https://thewho.com/',
        decimals: 6,
        source: {
          tools: ['Ligo'],
          location: 'https://ligolang.org/',
        },
        interfaces: ['TZIP-12', 'TZIP-16'],
        errors: [],
        views: [],
        tokens: {
          dynamic: [
            {
              big_map: 'token_metadata',
            },
          ],
        },
      }),
      'ascii',
    ).toString('hex'),
  }),
  total_supply: totalSupply,
};
