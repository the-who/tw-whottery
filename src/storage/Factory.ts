import { MichelsonMap } from '@taquito/michelson-encoder';
import { alice } from '../accounts';

export const factoryStorage = {
  counter: '0',
  token_list: MichelsonMap.fromLiteral({}),
  token_to_pool: MichelsonMap.fromLiteral({}),
  lambdas: MichelsonMap.fromLiteral({}),
  token_lambdas_b: MichelsonMap.fromLiteral({}),
  // pool_lambdas: MichelsonMap.fromLiteral({}),
  // token_lambdas: MichelsonMap.fromLiteral({}),
  // admin_lambdas: MichelsonMap.fromLiteral({}),
  admin_storage: {
    admin: alice.pkh,
    paused: false,
    pending_admin: null,
  },
  metadata: MichelsonMap.fromLiteral({}),
  // ledger: MichelsonMap.fromLiteral({}),
};
