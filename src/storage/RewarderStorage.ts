import { MichelsonMap } from '@taquito/michelson-encoder';
import { alice } from '../accounts';

export const rewarderStorage = {
  admin: alice.pkh,
  tokens: MichelsonMap.fromLiteral({}),
  pool_reward: MichelsonMap.fromLiteral({}),
  user_reward: MichelsonMap.fromLiteral({}),
};
