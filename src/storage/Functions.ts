export const poolFunctions = [
  {
    index: 0,
    name: "startPool",
  },
  {
    index: 1,
    name: "enterPool",
  },
  {
    index: 2,
    name: "exitPool",
  },
  {
    index: 3,
    name: "draw",
  },
  {
    index: 4,
    name: "round",
  },
];

export const adminFunctions  = [
  {
    index: 5,
    name: 'setPrnSrc',
  },
  {
    index: 6,
    name: 'setRoundTimeframe',
  },
  {
    index: 7,
    name: 'setRewardContract',
  },
];

const tokenFunctionsFA12 = [
  {
    index: 0,
    name: "transfer",
  },
  {
    index: 1,
    name: "approve",
  },
  {
    index: 2,
    name: "get_balance",
  },
  {
    index: 3,
    name: "get_allowance_to_contract",
  },
  {
    index: 4,
    name: "get_total_supply",
  },
];

const tokenFunctionsFA2 = [
  {
    index: 0,
    name: "transfer",
  },
  {
    index: 1,
    name: "update_operators",
  },
  {
    index: 2,
    name: "get_balance_of",
  },
];
export const tokenFunctions = {
  FA12: tokenFunctionsFA12,
  FA2: tokenFunctionsFA2,
};
