export const my = {
  sk: 'edskS2XFhECWyX6r1vTJZbYrZkJEoJTDfyMBZjcmJy2sK9ZTUBxXpsG5VpmWnrwZdtELpMLzbKctuvfzPQ6MGeRMTyeNuH94f2',
  pkh: 'tz1YnK3vLWLNTHWjUJL8tDNo2VNLwJNfof6R',
};
export const alice = {
  pkh: 'tz1Y4cyriDqDWhY26RnXbTAYJZA6o9cJUFaU',
  sk: 'edsk3RCboAkjDESWMQQ3d1vFWCebwHS26dYJhc7VG44kfTa6cfGjy5',
  pk: 'edpkuXUupVLzBMiXxeBgFE8m3R281wzTT43gKfJXdAND3ECJzfHkNf',
};
export const bob = {
  pkh: 'tz1WbVmtSz8UZqJTurRgUw5MrrGSkTzfgUv4',
  sk: 'edsk2tRJNpvPyKusnKZpwFuSxy6vJPRxXKtSCLJnhb9Takt8StmVur',
  pk: 'edpktrV8Bz6u4ZviXD2NJqnnpHkdE7gegxQQF5xWGgHazke1Hq9oke',
};
export const eve = {
  pkh: 'tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6',
  sk: 'edsk3Sb16jcx9KrgMDsbZDmKnuN11v4AbTtPBgBSBTqYftd8Cq3i1e',
  pk: 'edpku9qEgcyfNNDK6EpMvu5SqXDqWRLuxdMxdyH12ivTUuB1KXfGP4',
};
export const fred = {
  sk: 'edsk2t6oyjcWF9d4FwxFKabPBKitsanQb9dg2BsTMMCZPspZk5z784',
  pk: 'edpktfRBpxgsqzNjfuQfE87QQoZM2abHxKGJiUhBJTnJTHoCWgTNQ5',
  pkh: 'tz1UATkprhUYxBSXKXhd5brUi3BKp3WDkaxa',
};
