import * as fs from 'fs';

export const deployContract = async (Tezos, code, storage) => {
  try {
    const op = await Tezos.contract.originate({
      code,
      storage,
    });
    console.log(
      `Waiting for confirmation of origination for ${op.contractAddress}...`,
    );
    const contract = await op.contract();
    return contract;
  } catch (error) {
    // console.error(`Error: ${JSON.stringify(error, null, 2)}`);
    console.error(error);
  }
};
export const appendToEnv = (data) => {
  fs.appendFileSync('.env', data);
};
export const getLigoPath = () => {
  return `~/dev/tezosLigo/ligo.sh`;
};
